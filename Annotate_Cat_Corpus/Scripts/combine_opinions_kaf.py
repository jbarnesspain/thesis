import os
import argparse

def combine(kaf, opinions):
    opinion_idx = kaf.index('  <opinions>\n')
    final = kaf[:opinion_idx+1] + opinions[2:-1] + ['    </opinion>\n'] + kaf[-2:]
    return ''.join(final)

rev_dir = "/home/jeremy/NS/Keep/Temp/Thesis/Annotate_Cat_Corpus/Patrik-Revised/"
kaf_dir = "/home/jeremy/NS/Keep/Temp/Thesis/Annotate_Cat_Corpus/Compare_Annotations/patrik/Patrik-tag-to-kaf/"




if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-k', '--kaf_dir', help='directory where the original kaf files are found')
    parser.add_argument('-r', '--rev_dir', help='directory where the reviewed opinions are found (they must have the same name as the original kaf files)')
    parser.add_argument('-o', '--output_dir', help='directory where the new kafs combined with the revised opinions will be saved')

    args = parser.parse_args()

    # open reviewd opinions and remove any hidden files
    filenames = os.listdir(args.rev_dir)
    filenames = set([f for f in filenames if not f.endswith('~')])
    print('Opened {0} opinion files...'.format(len(filenames)))

    # open kafs and remove any hidden files
    kaf_filenames = os.listdir(args.kaf_dir)
    kaf_filenames = set([f for f in kaf_filenames if not f.endswith('~')])
    print('Opened {0} kaf files...'.format(len(kaf_filenames)))

    # get the intersection of both
    both = filenames.intersection(kaf_filenames)
    assert len(both) == len(filenames)
    print('Updating {0} kafs...'.format(len(both)))

    # get the kafs that won't be updated
    difference = kaf_filenames.difference(filenames)
    stable_kafs = [(f, open(os.path.join(args.kaf_dir, f)).read()) for f in difference]

    # get triples of (file_name, open_review, open_kaf)
    a = [(n, open(rev_dir + n).readlines(),
          open(kaf_dir + n).readlines()) for n in both]

    # replace the old opinion section with the revised one
    new = [(n, combine(kaf, rev)) for (n, rev, kaf) in a]
    # include the non-updated kafs for printing
    new += stable_kafs

    # save the kafs with the new opinion sections in output_dir
    if not os.path.exists(args.output_dir):
        os.mkdir(args.output_dir)
    for n, file in new:
        with open(os.path.join(args.output_dir, n), 'w') as out:
            out.write(file)
