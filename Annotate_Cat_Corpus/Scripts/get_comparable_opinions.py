import re

"""This is useful for comparing two sets of Kaf files that have been annotated
for sentiment. It extracts the text and opinion sections and puts them
in a new directory. You can then use 'pr -m -t -w 130 file1 file2' to compare
them in bash and make changes."""

def get_text(file):
    idx1 = file.index([line for line in file if '<text>' in line][0])+ 1
    idx2 = file.index([line for line in file if '</text>' in line][0]) -1
    text =  file[idx1:idx2]
    words = ' '.join([re.findall('>(.*)</wf>', line)[0] for line in text])
    return words

def get_opinions(file):
    idx1 = file.index([line for line in file if '<opinions>' in line][0])+ 1
    idx2 = file.index([line for line in file if '</opinions>' in line][0]) -1
    text =  ''.join(file[idx1:idx2])
    return text

def compare_text(file1, file2, outfile1, outfile2):
    text1 = get_text(file1)
    text2 = get_text(file2)
    op1 = get_opinions(file1)
    op2 = get_opinions(file2)
    with open(outfile1, 'w') as handle:
        handle.write(text1+'\n')
        handle.write('-'*80 + '\n')
        handle.write(op1 + '\n')
    with open(outfile2, 'w') as handle:
        handle.write(text2+'\n')
        handle.write('-'*80 + '\n')
        handle.write(op2 + '\n')


def get_compared_text(dir1, dir2, outdir):
    list1 = os.listdir(dir1)
    list2 = os.listdir(dir2)
    both = [f for f in list1 if f in list2]
    for filename in both:
        try:
            f1 = open(dir1 + filename).readlines()
            f2 = open(dir2 + filename).readlines()
            f3 = outdir + filename + '-jer'
            f4 = outdir + filename + '-pat'
            compare_text(f1, f2, f3, f4)
        except IndexError:
            continue
