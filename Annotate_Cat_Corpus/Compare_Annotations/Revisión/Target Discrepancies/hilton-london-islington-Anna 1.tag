w_1	El	el	DA0MS0		OpinionTarget	1	Opinion	3		0		0		0		0		0		0	0	true	1
w_2	bar	bar	NCMS000		OpinionTarget	1	Opinion	3		0		0		0		0		0		0	1	true	1
w_3	de	de	SPS00		OpinionTarget	1	Opinion	3		0		0		0		0		0		0	2	true	1
w_4	sushi	sushi	NCMS000		OpinionTarget	1	Opinion	3		0		0		0		0		0		0	3	true	1
w_5	Ichi	ichi	AQ0MS0		OpinionTarget	1	Opinion	3		0		0		0		0		0		0	4	true	1
w_6	no	no	RN		Negative	2	Opinion	3		0		0		0		0		0		0	5	true	1
w_7	té	tenir	VMIP3S0		Negative	2	Opinion	3		0		0		0		0		0		0	6	true	1
w_8	ni	ni	CC		Negative	2	Opinion	3		0		0		0		0		0		0	7	true	1
w_9	idea	idea	NCFS000		Negative	2	Opinion	3		0		0		0		0		0		0	8	true	1
w_10	de	de	SPS00		Negative	2	Opinion	3		0		0		0		0		0		0	9	true	1
w_11	com	com	CS		Negative	2	Opinion	3		0		0		0		0		0		0	10	true	1
w_12	fer	fer	VMN0000		Negative	2	Opinion	3		0		0		0		0		0		0	11	true	1
w_13	una	un	DI0FS0		Negative	2	Opinion	3		0		0		0		0		0		0	12	true	1
w_14	carta	carta	NCFS000		Negative	2	Opinion	3		0		0		0		0		0		0	13	true	1
w_15	que	que	PR0CN000		Negative	2	Opinion	3		0		0		0		0		0		0	14	true	1
w_16	pugui	poder	VMSP1S0		Negative	2	Opinion	3		0		0		0		0		0		0	15	true	1
w_17	adaptar-se	adaptar-se	NCFS000		Negative	2	Opinion	3		0		0		0		0		0		0	16	true	1
w_18	a	a	SPS00		Negative	2	Opinion	3		0		0		0		0		0		0	17	true	1
w_19	els	el	DA0MP0		Negative	2	Opinion	3		0		0		0		0		0		0	18	true	1
w_20	celíacs	celíac	AQ0MP0		Negative	2	Opinion	3		0		0		0		0		0		0	19	true	1
w_22	La	el	DA0FS0		OpinionTarget	4	Opinion	6		0		0		0		0		0		0	21	true	2
w_23	carta	carta	NCFS000		OpinionTarget	4	Opinion	6		0		0		0		0		0		0	22	true	2
w_24	de	de	SPS00		OpinionTarget	4	Opinion	6		0		0		0		0		0		0	23	true	2
w_25	el	el	DA0MS0		OpinionTarget	4	Opinion	6		0		0		0		0		0		0	24	true	2
w_26	servei	servei	NCMS000		OpinionTarget	4	Opinion	6		0		0		0		0		0		0	25	true	2
w_27	d'	de	SPS00		OpinionTarget	4	Opinion	6		0		0		0		0		0		0	26	true	2
w_28	habitacions	habitació	NCFP000		OpinionTarget	4	Opinion	6		0		0		0		0		0		0	27	true	2
w_29	no	no	RN		Negative	5	Opinion	6		0		0		0		0		0		0	28	true	2
w_30	especifica	especificar	VMIP3S0		Negative	5	Opinion	6		0		0		0		0		0		0	29	true	2
w_31	els	el	DA0MP0		Negative	5	Opinion	6		0		0		0		0		0		0	30	true	2
w_32	plats	plat	NCMP000		Negative	5	Opinion	6		0		0		0		0		0		0	31	true	2
w_33	sense	sense	SPS00		Negative	5	Opinion	6		0		0		0		0		0		0	32	true	2
w_34	gluten	gluten	NCMS000		Negative	5	Opinion	6		0		0		0		0		0		0	33	true	2
w_45	La	el	DA0FS0		OpinionTarget	7	Opinion	9		0		0		0		0		0		0	44	true	3
w_46	situació	situació	NCFS000		OpinionTarget	7	Opinion	9		0		0		0		0		0		0	45	true	3
w_47	de	de	SPS00		OpinionTarget	7	Opinion	9		0		0		0		0		0		0	46	true	3
w_48	l'	el	DA0CS0		OpinionTarget	7	Opinion	9		0		0		0		0		0		0	47	true	3
w_49	hotel	hotel	NCMS000		OpinionTarget	7	Opinion	9		0		0		0		0		0		0	48	true	3
w_50	és	ser	VSIP3S0			0	Opinion	9		0		0		0		0		0		0	49	true	3
w_51	perfecta	perfecte	AQ0FS0		StrongPositive	8	Opinion	9		0		0		0		0		0		0	50	true	3
w_52	per	per	SPS00		StrongPositive	8	Opinion	9		0		0		0		0		0		0	51	true	3
w_53	explorar	explorar	VMN0000		StrongPositive	8	Opinion	9		0		0		0		0		0		0	52	true	3
w_54	Londres	londres	NCFP000		StrongPositive	8	Opinion	9		0		0		0		0		0		0	53	true	3
w_56	Les	el	DA0FP0		OpinionTarget	10	Opinion	12		0		0		0		0		0		0	55	true	4
w_57	instal·lacions	instal·lació	NCFP000		OpinionTarget	10	Opinion	12		0		0		0		0		0		0	56	true	4
w_58	són	ser	VSIP3P0			0	Opinion	12		0		0		0		0		0		0	57	true	4
w_59	de	de	SPS00		StrongPositive	11	Opinion	12		0		0		0		0		0		0	58	true	4
w_60	primera	primer	AO0FS0		StrongPositive	11	Opinion	12		0		0		0		0		0		0	59	true	4
w_62	el	el	DA0MS0		OpinionTarget	13	Opinion	16		0		0		0		0		0		0	61	true	4
w_63	personal	personal	NCFS000		OpinionTarget	13	Opinion	16		0		0		0		0		0		0	62	true	4
w_64	és	ser	VSIP3S0			0	Opinion	16		0		0		0		0		0		0	63	true	4
w_65	atent	atent	AQ0MS0		Positive	14	Opinion	16		0		0		0		0		0		0	64	true	4
w_66	i	i	CC			0	Opinion	16		0		0		0		0		0		0	65	true	4
w_67	eficient	eficient	AQ0CS0		Positive	15	Opinion	16		0		0		0		0		0		0	66	true	4
w_69	El	el	DA0MS0		OpinionTarget	17	Opinion	19		0		0		0		0		0		0	68	true	5
w_70	restaurant	restaurant	NCMS000		OpinionTarget	17	Opinion	19		0		0		0		0		0		0	69	true	5
w_71	Joel	joel	NCMS000		OpinionTarget	17	Opinion	19		0		0		0		0		0		0	70	true	5
w_72	'	'	AQ0CS0		OpinionTarget	17	Opinion	19		0		0		0		0		0		0	71	true	5
w_73	s	segon	NCMN000		OpinionTarget	17	Opinion	19		0		0		0		0		0		0	72	true	5
w_74	és	ser	VSIP3S0			0	Opinion	19		0		0		0		0		0		0	73	true	5
w_75	una	un	DI0FS0		StrongPositive	18	Opinion	19		0		0		0		0		0		0	74	true	5
w_76	meravella	meravella	NCFS000		StrongPositive	18	Opinion	19		0		0		0		0		0		0	75	true	5
