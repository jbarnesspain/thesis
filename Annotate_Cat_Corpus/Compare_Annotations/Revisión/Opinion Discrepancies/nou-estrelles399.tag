w_1	Aire	aire	NCMS000		OpinionTarget	8	Opinion	3		0		0		0		0		0		0	0	true	1
w_2	condicionat	condicionar	VMP00SM		OpinionTarget	8	Opinion	3		0		0		0		0		0		0	1	true	1
w_3	habitació	habitació	NCFS000		OpinionTarget	8	Opinion	3		0		0		0		0		0		0	2	true	1
w_4	amb	amb	SPS00		Negative	2	Opinion	3		0		0		0		0		0		0	3	true	1
w_5	soroll	soroll	NCMS000		Negative	2	Opinion	3		0		0		0		0		0		0	4	true	1
w_7	SPA	spa	NCFS000		OpinionTarget	4	Opinion	7		0		0		0		0		0		0	6	true	2
w_8	a	a	SPS00		Negative	5	Opinion	7		0		0		0		0		0		0	7	true	2
w_9	temperatura	temperatura	NCFS000		Negative	5	Opinion	7		0		0		0		0		0		0	8	true	2
w_10	incorrecta	incorrecte	AQ0FS0		Negative	5	Opinion	7		0		0		0		0		0		0	9	true	2
w_11	(	(	NCFS000			0	Opinion	7		0		0		0		0		0		0	10	true	2
w_12	freda	fred	AQ0FS0		Negative	6	Opinion	7		0		0		0		0		0		0	11	true	2
w_14	Hamaques	hamaca	NCFP000		OpinionTarget	9	Opinion	11		0		0		0		0		0		0	13	true	2
w_15	habitació	habitació	NCFS000		OpinionTarget	9	Opinion	11		0		0		0		0		0		0	14	true	2
w_16	amb	amb	SPS00		Negative	10	Opinion	11		0		0		0		0		0		0	15	true	2
w_17	traus	trau	NCMP000		Negative	10	Opinion	11		0		0		0		0		0		0	16	true	2
w_18	a	a	SPS00		Negative	10	Opinion	11		0		0		0		0		0		0	17	true	2
w_19	el	el	DA0MS0		Negative	10	Opinion	11		0		0		0		0		0		0	18	true	2
w_20	teixit	teixit	NCMS000		Negative	10	Opinion	11		0		0		0		0		0		0	19	true	2
w_22	TV	tv	NC00000		OpinionTarget	12	Opinion	14		0		0		0		0		0		0	21	true	3
w_23	de	de	SPS00			0	Opinion	14		0		0		0		0		0		0	22	true	3
w_24	grandària	grandària	NCFS000			0	Opinion	14		0		0		0		0		0		0	23	true	3
w_25	una	un	DI0FS0		Negative	13	Opinion	14		0		0		0		0		0		0	24	true	3
w_26	mica	mica	NCFS000		Negative	13	Opinion	14		0		0		0		0		0		0	25	true	3
w_27	petita	petit	AQ0FS0		Negative	13	Opinion	14		0		0		0		0		0		0	26	true	3
w_29	Cobrament	cobrament	NCMS000		OpinionTarget	17	Opinion	19		0		0		0		0		0		0	28	true	4
w_30	de	de	SPS00		OpinionTarget	17	Opinion	19		0		0		0		0		0		0	29	true	4
w_31	els	el	DA0MP0		OpinionTarget	17	Opinion	19		0		0		0		0		0		0	30	true	4
w_32	entreteniments	entreteniment	NCMP000		OpinionTarget	17	Opinion	19		0		0		0		0		0		0	31	true	4
w_33	de	de	SPS00		OpinionTarget	17	Opinion	19		0		0		0		0		0		0	32	true	4
w_34	el	el	DA0MS0		OpinionTarget	17	Opinion	19		0		0		0		0		0		0	33	true	4
w_35	restaurant	restaurant	NCMS000		OpinionTarget	17	Opinion	19		0		0		0		0		0		0	34	true	4
w_36	sense	sense	SPS00		Negative	18	Opinion	19		0		0		0		0		0		0	35	true	4
w_37	avisar	avisar	VMN0000		Negative	18	Opinion	19		0		0		0		0		0		0	36	true	4
w_48	Manca	manca	NCFS000		Negative	26	Opinion	27		0		0		0		0		0		0	47	true	6
w_49	de	de	SPS00			0	Opinion	27		0		0		0		0		0		0	48	true	6
w_50	productes	producte	NCMP000		OpinionTarget	25	Opinion	27		0		0		0		0		0		0	49	true	6
w_51	per	per	SPS00		OpinionTarget	25	Opinion	27		0		0		0		0		0		0	50	true	6
w_52	a	a	SPS00		OpinionTarget	25	Opinion	27		0		0		0		0		0		0	51	true	6
w_53	celíacs	celíac	AQ0MP0		OpinionTarget	25	Opinion	27		0		0		0		0		0		0	52	true	6
w_54	.	.	NCMS000			0		0		0		0		0		0		0		0	53	true	6
w_55	Entorn	entornar	VMIP1S0		OpinionTarget	28	Opinion	30		0		0		0		0		0		0	54	true	7
w_56	molt	molt	RG		StrongPositive	29	Opinion	30		0		0		0		0		0		0	55	true	7
w_57	cuidat	cuidat	NCMS000		StrongPositive	29	Opinion	30		0		0		0		0		0		0	56	true	7
w_75	Amabilitat	amabilitat	NCFS000		Positive	31	Opinion	33		0		0		0		0		0		0	74	true	11
w_76	de	de	SPS00			0	Opinion	33		0		0		0		0		0		0	75	true	11
w_77	el	el	DA0MS0		OpinionTarget	32	Opinion	33		0		0		0		0		0		0	76	true	11
w_78	personal	personal	AQ0CS0		OpinionTarget	32	Opinion	33		0		0		0		0		0		0	77	true	11
w_80	Buffet	buffet	RG		OpinionTarget	34	Opinion	36		0		0		0		0		0		0	79	true	12
w_81	d'	de	SPS00		OpinionTarget	34	Opinion	36		0		0		0		0		0		0	80	true	12
w_82	esmorzar	esmorzar	VMN0000		OpinionTarget	34	Opinion	36		0		0		0		0		0		0	81	true	12
w_83	perfecte	perfecte	AQ0MS0		StrongPositive	35	Opinion	36		0		0		0		0		0		0	82	true	12
