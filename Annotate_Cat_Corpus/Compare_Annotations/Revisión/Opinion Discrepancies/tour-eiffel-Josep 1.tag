w_1	L'	el	DA0CS0		OpinionTarget	1	Opinion	3		0		0		0		0		0		0	0	true	1
w_2	hotel	hotel	NCMS000		OpinionTarget	1	Opinion	3		0		0		0		0		0		0	1	true	1
w_3	tot	tot	RG			0	Opinion	3		0		0		0		0		0		0	2	true	1
w_4	i	i	CC			0	Opinion	3		0		0		0		0		0		0	3	true	1
w_5	dir-se	dir-se	NCFS000			0	Opinion	3		0		0		0		0		0		0	4	true	1
w_6	Le	le	AQ0CS0			0	Opinion	3		0		0		0		0		0		0	5	true	1
w_7	Marais	marais	NCMP000			0	Opinion	3		0		0		0		0		0		0	6	true	1
w_8	no	no	RN		Negative	2	Opinion	3		0		0		0		0		0		0	7	true	1
w_9	està	estar	VMIP3S0		Negative	2	Opinion	3		0		0		0		0		0		0	8	true	1
w_10	situat	situar	VMP00SM		Negative	2	Opinion	3		0		0		0		0		0		0	9	true	1
w_11	ben	ben	RG		Negative	2	Opinion	3		0		0		0		0		0		0	10	true	1
w_12	bé	bé	RG		Negative	2	Opinion	3		0		0		0		0		0		0	11	true	1
w_13	a	a	SPS00		Negative	2	Opinion	3		0		0		0		0		0		0	12	true	1
w_14	Le	le	AQ0CS0		Negative	2	Opinion	3		0		0		0		0		0		0	13	true	1
w_15	Marais	marais	NCMP000		Negative	2	Opinion	3		0		0		0		0		0		0	14	true	1
w_18	molt	molt	RG		StrongPositive	4	Opinion	5		0		0		0		0		0		0	17	true	2
w_19	ben	ben	RG		StrongPositive	4	Opinion	5		0		0		0		0		0		0	18	true	2
w_20	comunicat	comunicat	NCMS000		StrongPositive	4	Opinion	5		0		0		0		0		0		0	19	true	2
w_21	per	per	SPS00		StrongPositive	4	Opinion	5		0		0		0		0		0		0	20	true	2
w_22	metro	metro	NCMS000		StrongPositive	4	Opinion	5		0		0		0		0		0		0	21	true	2
w_31	em	jo	PP1CS000		OpinionHolder	6	Opinion	8		0		0		0		0		0		0	30	true	2
w_32	vaig	anar	VAIP1S0		OpinionHolder	6	Opinion	8		0		0		0		0		0		0	31	true	2
w_33	trobar	trobar	VMN0000		Negative	7	Opinion	8		0		0		0		0		0		0	32	true	2
w_34	amb	amb	SPS00		Negative	7	Opinion	8		0		0		0		0		0		0	33	true	2
w_35	aquesta	aquest	DD0FS0		Negative	7	Opinion	8		0		0		0		0		0		0	34	true	2
w_36	decepció	decepció	NCFS000		Negative	7	Opinion	8		0		0		0		0		0		0	35	true	2
w_38	L'	el	DA0CS0		OpinionTarget	9	Opinion	11		0		0		0		0		0		0	37	true	3
w_39	hotel	hotel	NCMS000		OpinionTarget	9	Opinion	11		0		0		0		0		0		0	38	true	3
w_40	és	ser	VSIP3S0			0	Opinion	11		0		0		0		0		0		0	39	true	3
w_41	molt	molt	RG		StrongPositive	10	Opinion	11		0		0		0		0		0		0	40	true	3
w_42	confortable	confortable	AQ0CS0		StrongPositive	10	Opinion	11		0		0		0		0		0		0	41	true	3
w_45	un	un	DI0MS0		OpinionTarget	12	Opinion	14		0		0		0		0		0		0	44	true	3
w_46	disseny	disseny	NCMS000		OpinionTarget	12	Opinion	14		0		0		0		0		0		0	45	true	3
w_47	molt	molt	RG		StrongPositive	13	Opinion	14		0		0		0		0		0		0	46	true	3
w_48	atractiu	atractiu	AQ0MS0		StrongPositive	13	Opinion	14		0		0		0		0		0		0	47	true	3
w_50	Les	el	DA0FP0		OpinionTarget	15	Opinion	18		0		0		0		0		0		0	49	true	4
w_51	habitacions	habitació	NCFP000		OpinionTarget	15	Opinion	18		0		0		0		0		0		0	50	true	4
w_52	són	ser	VSIP3P0			0	Opinion	18		0		0		0		0		0		0	51	true	4
w_53	petites	petit	AQ0FP0		Negative	16	Opinion	18		0		0		0		0		0		0	52	true	4
w_54	però	però	CC			0	Opinion	18		0		0		0		0		0		0	53	true	4
w_55	estan	estar	VMIP3P0			0	Opinion	18		0		0		0		0		0		0	54	true	4
w_56	molt	molt	RG		StrongPositive	17	Opinion	18		0		0		0		0		0		0	55	true	4
w_57	ben	ben	RG		StrongPositive	17	Opinion	18		0		0		0		0		0		0	56	true	4
w_58	equipades	equipar	VMP00PF		StrongPositive	17	Opinion	18		0		0		0		0		0		0	57	true	4
w_60	el	el	DA0MS0		OpinionTarget	19	Opinion	21		0		0		0		0		0		0	59	true	4
w_61	llit	llit	NCMS000		OpinionTarget	19	Opinion	21		0		0		0		0		0		0	60	true	4
w_62	és	ser	VSIP3S0			0	Opinion	21		0		0		0		0		0		0	61	true	4
w_63	molt	molt	RG		StrongPositive	20	Opinion	21		0		0		0		0		0		0	62	true	4
w_64	còmode	còmode	AQ0MS0		StrongPositive	20	Opinion	21		0		0		0		0		0		0	63	true	4
w_66	El	el	DA0MS0		OpinionTarget	22	Opinion	24		0		0		0		0		0		0	65	true	5
w_67	servei	servei	NCMS000		OpinionTarget	22	Opinion	24		0		0		0		0		0		0	66	true	5
w_68	de	de	SPS00		OpinionTarget	22	Opinion	24		0		0		0		0		0		0	67	true	5
w_69	transfer	transferir	VMIP3S0		OpinionTarget	22	Opinion	24		0		0		0		0		0		0	68	true	5
w_70	de	de	SPS00		OpinionTarget	22	Opinion	24		0		0		0		0		0		0	69	true	5
w_71	l'	el	DA0CS0		OpinionTarget	22	Opinion	24		0		0		0		0		0		0	70	true	5
w_72	hotel	hotel	NCMS000		OpinionTarget	22	Opinion	24		0		0		0		0		0		0	71	true	5
w_73	és	ser	VSIP3S0			0	Opinion	24		0		0		0		0		0		0	72	true	5
w_74	molt	molt	RG		StrongPositive	23	Opinion	24		0		0		0		0		0		0	73	true	5
w_75	bo	bo	AQ0MS0		StrongPositive	23	Opinion	24		0		0		0		0		0		0	74	true	5
