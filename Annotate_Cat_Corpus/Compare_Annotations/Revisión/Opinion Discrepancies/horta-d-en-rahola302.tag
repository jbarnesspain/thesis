w_1	Quedàvem	quedar	VMII1P0		OpinionHolder	1	Opinion	3		0		0		0		0		0		0	0	true	1
w_2	molt	molt	RG		Negative	2	Opinion	3		0		0		0		0		0		0	1	true	1
w_3	lluny	lluny	RG		Negative	2	Opinion	3		0		0		0		0		0		0	2	true	1
w_4	de	de	SPS00		Negative	2	Opinion	3		0		0		0		0		0		0	3	true	1
w_5	cap	cap	DI0CS0		Negative	2	Opinion	3		0		0		0		0		0		0	4	true	1
w_6	zona	zona	NCFS000		Negative	2	Opinion	3		0		0		0		0		0		0	5	true	1
w_7	de	de	SPS00		Negative	2	Opinion	3		0		0		0		0		0		0	6	true	1
w_8	passeig	passeig	NCMS000		Negative	2	Opinion	3		0		0		0		0		0		0	7	true	1
w_14	depens	dependre	VMIP2S0		Negative	4	Opinion	5		0		0		0		0		0		0	13	true	1
w_15	de	de	SPS00		Negative	4	Opinion	5		0		0		0		0		0		0	14	true	1
w_16	el	el	DA0MS0		Negative	4	Opinion	5		0		0		0		0		0		0	15	true	1
w_17	cotxe	cotxe	NCMS000		Negative	4	Opinion	5		0		0		0		0		0		0	16	true	1
w_18	d'	de	SPS00		Negative	4	Opinion	5		0		0		0		0		0		0	17	true	1
w_19	una	un	DI0FS0		Negative	4	Opinion	5		0		0		0		0		0		0	18	true	1
w_20	manera	manera	NCFS000		Negative	4	Opinion	5		0		0		0		0		0		0	19	true	1
w_21	exagerada	exagerada	NCFS000		Negative	4	Opinion	5		0		0		0		0		0		0	20	true	1
w_23	El	el	DA0MS0		StrongNegative	6	Opinion	8		0		0		0		0		0		0	22	true	2
w_24	pitjor	pitjor	AQ0CS0		StrongNegative	6	Opinion	8		0		0		0		0		0		0	23	true	2
w_25	és	ser	VSIP3S0			0	Opinion	8		0		0		0		0		0		0	24	true	2
w_26	l'	el	DA0CS0		OpinionTarget	7	Opinion	8		0		0		0		0		0		0	25	true	2
w_27	horari	horari	NCMS000		OpinionTarget	7	Opinion	8		0		0		0		0		0		0	26	true	2
w_28	d'	de	SPS00		OpinionTarget	7	Opinion	8		0		0		0		0		0		0	27	true	2
w_29	arribada	arribada	NCFS000		OpinionTarget	7	Opinion	8		0		0		0		0		0		0	28	true	2
w_30	i	i	CC		OpinionTarget	7	Opinion	8		0		0		0		0		0		0	29	true	2
w_31	sortida	sortida	NCFS000		OpinionTarget	7	Opinion	8		0		0		0		0		0		0	30	true	2
w_32	de	de	SPS00		OpinionTarget	7	Opinion	8		0		0		0		0		0		0	31	true	2
w_33	l'	el	DA0CS0		OpinionTarget	7	Opinion	8		0		0		0		0		0		0	32	true	2
w_34	apartament	apartament	NCMS000		OpinionTarget	7	Opinion	8		0		0		0		0		0		0	33	true	2
w_36	Acabes	acabar	VMIP2S0		Negative	9	Opinion	10		0		0		0		0		0		0	35	true	3
w_37	malbaratant	malbaratar	VMG0000		Negative	9	Opinion	10		0		0		0		0		0		0	36	true	3
w_38	els	el	DA0MP0		Negative	9	Opinion	10		0		0		0		0		0		0	37	true	3
w_39	dies	dia	NCMP000		Negative	9	Opinion	10		0		0		0		0		0		0	38	true	3
w_40	d'	de	SPS00		Negative	9	Opinion	10		0		0		0		0		0		0	39	true	3
w_41	estada	estada	NCFS000		Negative	9	Opinion	10		0		0		0		0		0		0	40	true	3
w_43	Hem	haver	VAIP1P0		OpinionHolder	11	Opinion	14		0		0		0		0		0		0	42	true	4
w_44	estat	ser	VSP00SM			0	Opinion	14		0		0		0		0		0		0	43	true	4
w_45	molt	molt	RG		StrongPositive	12	Opinion	14		0		0		0		0		0		0	44	true	4
w_46	tranquils	tranquil	AQ0MP0		StrongPositive	12	Opinion	14		0		0		0		0		0		0	45	true	4
w_47	a	a	SPS00			0	Opinion	14		0		0		0		0		0		0	46	true	4
w_48	l'	el	DA0CS0		OpinionTarget	13	Opinion	14		0		0		0		0		0		0	47	true	4
w_49	apartament	apartament	NCMS000		OpinionTarget	13	Opinion	14		0		0		0		0		0		0	48	true	4
w_51	Era	ser	VSII1S0			0	Opinion	17		0		0		0		0		0		0	50	true	5
w_52	còmode	còmode	AQ0MS0		Positive	15	Opinion	17		0		0		0		0		0		0	51	true	5
w_53	i	i	CC			0	Opinion	17		0		0		0		0		0		0	52	true	5
w_54	estava	estar	VMII3S0			0	Opinion	17		0		0		0		0		0		0	53	true	5
w_55	molt	molt	RG		StrongPositive	16	Opinion	17		0		0		0		0		0		0	54	true	5
w_56	a	a	SPS00		StrongPositive	16	Opinion	17		0		0		0		0		0		0	55	true	5
w_57	prop	prop	RG		StrongPositive	16	Opinion	17		0		0		0		0		0		0	56	true	5
w_58	de	de	SPS00		StrongPositive	16	Opinion	17		0		0		0		0		0		0	57	true	5
w_59	la	el	DA0FS0		StrongPositive	16	Opinion	17		0		0		0		0		0		0	58	true	5
w_60	platja	platja	NCFS000		StrongPositive	16	Opinion	17		0		0		0		0		0		0	59	true	5
