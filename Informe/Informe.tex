
\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage[natbib=true, backend=bibtex, style=authoryear,
citestyle=authoryear, isbn=false, url=false, doi=false]{biblatex}
\bibliography{/home/jeremy/Documentos/Thesis/Informe/Informe.bib}
\usepackage[]{csquotes}
\renewcommand{\baselinestretch}{1.5}
\usepackage{times}

%opening
\title{Report of Progress on Doctoral Research in Cross-lingual Sentiment Analysis}
\date{April 19, 2016}
\author{Jeremy Barnes}

\begin{document}

\maketitle

\begin{abstract}
Cross-lingual sentiment analysis (CLSA) seeks to leverage data from a data-rich source language in order to perform sentiment classification in a target language which lacks resources. Distributed representations of words (word embeddings) have proven effective in monolingual sentiment analysis but less research has been conducted on their use in CLSA. We conduct extensive experiments using monolingual word embeddings in an attempt to find a way to map vectors from source to target languages. This would serve as an alternative to the machine translation methods most often used in CLSA. However, we will demonstrate that such a simple mapping scheme is not able to capture the semantic regularities between languages in a way that permits accurate classification of cross-lingual data.
\end{abstract}

\section{Introduction}
The aim of these experiments is to find a way to leverage English data to perform fine grained cross-lingual sentiment analysis in a target language such as Spanish or Catalan without using any parallel data. We based our approach on \cite{Mikolov2013d}, who proposed the use of a \textit{translation matrix} in order to bridge the gap between two monolingual distributional language models. They then used this mapping scheme to improve the performance of statistical machine translation systems. Specifically, they were able to fill gaps in translation dictionaries and phrase tables by 'translating' the vector representation from the source language space to the target language space and searching for the nearest neighbor. Our hypothesis is that a translation matrix such as this could also be used to perform cross-lingual sentiment analysis. This would be significant as it would reduce our dependency on parallel data and allow us to perform sentiment analysis in languages with very few resources.

The remainder of this paper is organized as follows. Section 2 gives details on each of the experiments that were conducted. Section 3 is a discussion of the results and directions for future research.
\\
\\
\\
\section{Experiments}
\subsection{Data Sets}

The word embeddings used in the experiments are trained on corpora obtained from Wikipedia dumps in each respective language (Table 1). The Wikepedia corpora are first cleaned using a python script and then sentence and word tokenized using nltk (\cite{Loper2002}) and freeling (\cite{Padro2010}). Following \cite{Mikolov2013d}, we create two sets of monolingual word embeddings. We use the publicly available GoogleNews vectors (\cite{Mikolov2013}) for the English monolingual 300-dimension word embeddings and create all other word embeddings using \textit{gensim} (\cite{rehurek_lrec}). During training word embeddings are created using a window of 10 words.


The data utilized to train the sentiment analysis models are the English and Spanish OpeNER sentiment corpora (\cite{Agerri2013}) as well as a small Catalan corpus modeled on these two. These are corpora that have been annotated at aspect level. As such, when training a classifier, rather than training on the complete sentence, the opinion unit is used. The statistics on these corpora are given in Table 2. 





\begin{table}[h]
\centering
 \begin{tabular}{|c|c|c|c|} 
 \hline
 OpeNER Corpora & English & Spanish & Catalan \\ 
 \hline\hline
 Number of files & 10,976 & 2749 & 879 \\ 
 Number of sentences & 118,900,197 &26,777,415 & 9,614,045 \\
 Number of tokens & 2,055,786,401 & 506,612,108 & 162,823,686 \\ 
 \hline
 \end{tabular}
 \caption{Statistics of Wikipedia Corpora}
 \label{table:1}
\end{table}


\begin{table}[h]
\centering
 \begin{tabular}{|c|c|c|c|} 
 \hline
 Wikipedia Corpora & English & Spanish & Catalan \\  
 \hline\hline
 Training Examples & 2780 & 2991 & 992 \\ 
 – Strong Pos \% & – 23.38\% & – 29\% & – 27.33\% \\
 – Pos\% & – 46.08\% & – 50.34\% & – 34.16\% \\  
 – Neg\% & – 25.61\% & – 17.41\% & – 33.3\% \\
 – Strong Neg \% & – 4.93\% & – 3.01\% & – 5.21\% \\
 \hline\hline
 Test examples & 929 & 999 & 310 \\
 – Strong Pos \% & – 23.36\% &  – 29.23\% & – 27.1\% \\
 – Pos\% & – 46.07\% & – 50.34\% & – 34.19\% \\
 – Neg\% & – 25.62\% & – 17.42\% & – 33.48\% \\
 – Strong Neg \% & – 4.95\% & – 3.00\% & – 5.48\% \\
 \hline
 \end{tabular}
 \caption{Statistics of OpeNER Corpora}
 \label{table:2}
\end{table}



\subsection{Experiment 1}
\subsubsection{Experimental Layout}


The second part of the experiment consisted of trying to create a \(\Re^{300 x 300}\)  matrix which would serve as a way to bridge the gap between the Catalan and English word vectors. The desired effect is that the dot product of a Catalan word vector and the translation matrix would be very similar to the English word vector, as in the following:

\begin{equation}
\Re^{interessant}\circ TranslationMatrix\approx\Re^{interesting}
\end{equation}			

In order to create this  translation matrix \textit{W}, a bilingual dictionary is compiled from the 8000 most common words in the English Wikipedia corpus. These words are then translated using Bing Translator. Now, given a word pair and their associated vector representations \{\textit{\(x_i, z_i\)}\}, we would like to optimize the following for our training vocabulary of length n:

\begin{equation}
\min_{W} \sum^{n}_{i=1} \parallel x_i - z_i \parallel^2 
\end{equation}
						
	 	

We train a linear algorithm to optimize this cost function using Minibatch Stochastic Gradient Descent with a mini-batch size of 20, a learning rate of \(1e^{-4}\) and run for 50,000 epochs. Loss and validation loss are shown in Figure 1.

\begin{figure}[h]
\centering
\includegraphics[width=.8\linewidth]{/home/jeremy/Escritorio/LSTM_Sentiment_Analysis/Graphs/MappingSpanish800English100}
\caption{Loss and validation loss during the training of the translation matrix}
\label{fig:1}
\end{figure}




After creating the transition matrix \textit{W}, we then test the effectiveness of this matrix translation to enable cross-lingual sentiment analysis. We use the English and Catalan OpeNER corpora and create three datasets. For each phrase in the corpora, we take the vectors for each word from our pretrained word embeddings and create a final vector representation of the phrase by summing these individual word vectors\footnote[1]{This is a rather naive way of combining vectors, but the learning algorithms require the input to be of the same size. Concatenation would lead to different sized vectors depending on the number of words in a phrase. The results of the monolingual classifiers also showed that enough sentiment information is retained in order to create a reasonably accurate monolingual classifier.}. We now have a dataset with training instances such as {\(\{x_i:y_i\}\)}, where \(x_i\) is a 300 dimension vector and \(y_i\) is its corresponding label (Strong Positive, Positive, Negative, Strong Negative) represented as a one-hot vector. 

First an English and Catalan classifier are trained with monolingual training instances in order to provide a baseline for the cross-lingual classifier. The Catalan test set is converted using our translation matrix \textit{W}, and we can now test the results.

The classifiers used are neural networks with two hidden layers (\(\Re^{1000}, \Re^{1200}\)).  The hidden layers' activation function is a rectified linear unit (\textit{relu}) and the output layer gives a softmax score of probability over the classes. The cost function used is categorical crossentropy and the network is trained for 100 epochs using the \textit{Adam} algorithm (\cite{Kingma2014a}), which adaptively changes the learning rate of Stochastic Gradient Descent in order to converge more quickly. Table 3  shows the accuracy of the classifiers on each test set. Table 4 shows the precision, recall and F1 score of the English and Crosslingual classifiers.
\\


\begin{table}[h!]
\centering
 \begin{tabular}{|c|c|c|c|} 
 \hline
  & English & Catalan & Cross-lingual \\ [0.5ex] 
 \hline
 Accuracy on test set & 80.4\% & 70.65\% & 34.19\% \\ [1ex] 
 \hline
 \end{tabular}
 \caption{Accuracy of the 3 classifiers during the first experiment}
 \label{table:3}
\end{table}

\begin{table}[h!]
\centering
 \begin{tabular}{|c|c|c|c|c|} 
 \hline
  \textbf{English monolingual}& Strong Negative  & Negative & Positive & Strong Positive \\ [0.5ex] 
  
 \hline
 Precision &.81& .76& .82& .78 \\
  \hline
  Recall &.67 & .73 & .81 & .86 \\
  \hline
  F1 Score & .73 & .75 & .82 & .82 \\
  \hline
 \textbf{Cross-lingual} & --  & -- & -- & -- \\ [0.5ex] 
 \hline
 Precision & 0 & .12 &.32 & .18\\
 \hline
 Recall & 0 & .02 & .85 & .02\\
 \hline
 F1 Score & 0 & .03 & .47 & .04\\ [1ex] 
 \hline
 \end{tabular}
 \caption{Precision, Recall and F1 of English and cross-lingual classifiers}
 \label{table:4}
\end{table}

From these results, it is clear that in its most basic form, the mapping strategy put forth in \cite{Mikolov2013d} does not work as well for sentiment analysis as it did for helping machine translation. The low precision and recall in the Strong Negative and Strong positive categories are most likely due to few examples being present. The noise introduced by the translation matrix reduces the number of effective training examples. The Negative category, however, is well-represented in the corpora. The English trained classifier seems to nearly always prefer the Positive category on the cross-lingual test set.

\subsubsection{Discussion of First Results}

Assuming that the mapping strategy is useful, there are several possible reasons for the results in this first experiment. The first and most obvious reason would be the small size of the Catalan OpeNER dataset. The size of the Catalan test set is particularly small, which could contribute to the inaccuracy of the cross-lingual model.

 \cite{Mikolov2013d} suggest that the dimension of the vectors is important. In fact, they state that the word vectors trained on the source language should be 2 to 4 times target than the target vectors. Therefore it could be a matter of finding the right ratio of vector size.

Another factor could be the way that the vectors used to train the classification algorithm were created. That is, they were taken as the sum of all of the vectors in the phrase. The translation matrix would therefore be applied to a vector which is the sum of the words, when it was optimized for word-to-word translation. 


Finally, it may be that the words used to train the translation matrix (the 5000 most common words from the English Wikipedia corpus) do not coincide with the words that are found in the OpeNER corpora. 

All of these doubts will be dealt with in the following experiments.

\subsection{Experiment 2}
In experiment 2, it was important to rule out the possibility that the problem was the small Catalan dataset. Therefore, we incorporate the larger Spanish OpeNER dataset to test this possibility under the same conditions. Different combinations of vector size for the pre-trained word embeddings were also used to test the importance of the vector dimensions on the mapping. All of the word embeddings except for En-300 were trained on the wikipedia corpora using Gensim with a window of 10 words and 7 negative examples, which was reported to have the best results by \cite{Mikolov2013}.  The bilingual dictionary contained 7000 word pairs. Results of experiment 2 are shown in Table 5.

\begin{table}[h!]
\centering
 \begin{tabular}{|c|c|c|c|} 
 \hline
  & English& Spanish & Cross-lingual \\ [0.5ex] 
 \hline
  Vector Dimensions & & & \\
 \hline
 En-100, Sp-800 & 75.24\% & 71.97\% & 51.65\% \\ 
 \hline
 En-100, Sp-300 & 73.3\% & 73.9\% & 53.55\% \\ 
 \hline
 En-300, Sp-800 & 78.9\% & 73.07\% & 45.45\% \\ 
 \hline
 En-300, Sp-300 & 79.76\% & 71.27\% & 36.73\% \\ 
 \hline
 En-300 on 
 Spanish Test
 No mapping & - & - & 46.95\% \\
 [1ex] 
 \hline
 \end{tabular}
 \caption{Results of Experiment 2: Spanish OpeNER dataset – 4 class}
 \label{table:5}
\end{table}

As we can see, the use of the Spanish dataset and different dimensions had no real effect on the cross-lingual model. In fact, evaluating the English model directly on the Spanish test set gave results that are better than some of the results after mapping. This seems to indicate that the translation matrix is actually creating more distance between the word vectors in English and Spanish, rather than pulling the word vectors for translation pairs into a similar area in vector space. This intuition seems to be corroborated by plotting the cosine distance as a function of the training examples when optimizing the training matrix, shown in Figure 2. The cosine distance between vectors increases after training rather than decreasing.

\begin{figure}[h]
\centering
\includegraphics[width=.8\linewidth]{/home/jeremy/Escritorio/LSTM_Sentiment_Analysis/Graphs/Keras_Linear_Mapping}
\caption{Cosine distance plotted as a function of training examples}
\label{fig:2}
\end{figure}


\subsection{Experiment 3}
In experiment 3, it was important try a different method to create the vectors used to train the classifier. Therefore, instead of summing them, we took the average of the vectors in the phrase to create the final sentence vector. Results are shown in Table 6.


\begin{table}[h!]
\centering
 \begin{tabular}{|c|c|c|c|} 
 \hline
  & English & Spanish & Cross-lingual \\ [0.5ex] 
 \hline
  Vector Dimensions & & & \\
 \hline
 En-100, Sp-800 & 74.81\% & 74.07\% & 17.82\% \\ 
 \hline
 En-100, Sp-300 & 75.02\% & 73.67\% & 17.82\% \\ 
 \hline
 En-300, Sp-800 & 77.18\% & 71.47\% & 49.64\% \\ 
 \hline
 En-300, Sp-300 & 78.04\% & 73.27\% & 50.45\% \\ 
 \hline
 En-300 on 
 Spanish Test
 No mapping & - & - & 49.05\% \\
 [1ex] 
 \hline
 \end{tabular}
 \caption{Results of Experiment 3: using average of vectors instead of sum of vectors}
 \label{table:6}
\end{table}

As we can see, the performance of the cross-lingual model dropped severely in the first two experiments and was hardly better than evaluating the Spanish test set with the English model.

\subsection{Experiment 4}

During the initial experiment, it appeared that summing or averaging the vectors of a sentence was one of the factors that could have contributed to the inefficiency of the cross-lingual model, especially given that the translation matrix was optimized for mapping between words, not entire phrases. In order to test this possibility, we chose to use a different machine learning model which could take variable-length sequences, thereby removing the need to collapse each sentence into a single vector. Following a tutorial by Pierre Luc Carrier and Kyunghyun Cho\footnote[2]{http://deeplearning.net/tutorial/lstm.html}, we chose an LSTM with 400 dimension hidden layer. The final state of the LSTM was then sent to an output layer which output a softmax probability over the 4 classes. This is a slightly modified version of the LSTM found in the tutorial and was used for simplicity, as the aim of the experiment was to see if LSTMs could enable the use of our mapping scheme. The results of experiment 4 are shown in Table 7.


\begin{table}[h!]
\centering
 \begin{tabular}{|c|c|c|c|} 
 \hline
  & English & Spanish & Cross-lingual \\ [0.5ex] 
 \hline
  Vector Dimensions & & & \\
 \hline
 En-100, Sp-800 & 64.3\% & 74.0\% & 50.0\% \\ 
 \hline
 En-300, Sp-800 & 71.3\% & 73.6\% & 43.8\% \\ 
 \hline
 En-300, Sp-300 & 60.38\% & 75.1\% & 43.54\% \\ 
 \hline
 En-300 on 
 Spanish Test
 No mapping & - & - & 38.94\% \\
 [1ex] 
 \hline
 \end{tabular}
 \caption{Results of experiment 4 – 2 layer LSTM}
 \label{table:7}
\end{table}


 Again, the cross-lingual model shows no signs of improvement, with performances below the weakest baseline of choosing the most common class. We also implemented a deep bidirectional LSTM, following \cite{Irsoy2014a}, but the results were similar and the model was more computationally expensive. These results are not shown.

\subsection{Experiment 5}

In our final experiment, we change algorithms in order to test whether a more powerful learning algorithm can more accurately capture the regularities between two vector space language models. Instead of training a linear algorithm, we train a 2 layer neural network. \cite{Mikolov2013d} state that a neural network worked as well as the linear algorithm in their research, but do not give details as they preferred the linear approach. Our network consisted of layers which were $\Re^{500}, \Re^{500}$, the loss function used during training was mean squared error and the optimizer used was the \textit{Adam} algorithm (\cite{Kingma2014a}). The results are shown in Table 8.



\begin{table}[h!]
\centering
 \begin{tabular}{|c|c|c|c|} 
 \hline
  & English & Spanish & Crosslingual \\ [0.5ex] 
 \hline
  Vector Dimensions & & & \\
 \hline
 En-100, Sp-800 & 74.27\% & 73.27\% & 50.35\% \\ 
 \hline
 En-300, Sp-800 & 79.87\% & 71.97\% & 50.35\% \\ 
 \hline
 En-300, Sp-300 & 79.33\% & 75.08\% & 50.35\% \\ 
 \hline
 En-300 on 
 Spanish Test
 No mapping & - & - & 50.35\% \\
 [1ex] 
 \hline
 \end{tabular}
 \caption{Results of experiment 5 - Neural mapping scheme}
 \label{table:8}
\end{table}

It is quite clear from the data that the cross-lingual classifier is not able to make use of the training data to classify the cross-lingual data accurately.

\section{Discussion and Further Research}
While \cite{Mikolov2013d} were able to leverage a simple mapping strategy between word embeddings created from large monolingual datasets in order to fill the gaps in translation dictionaries, given the poor results of these experiments, it seems unlikely that this same strategy can effectively capture the complex relationship between target and source language word vectors in a way that is useful for sentiment analysis. This is likely due to the different purposes of the mapping strategy in each approach. In \cite{Mikolov2013d}, this bilingual distributed representation had an auxiliary role; mainly filling in the gaps in a pre-existing translation dictionary. In our approach, however, all of the weight of correctly classifying a phrase fell on the accuracy of the mapping scheme. Therefore, it seems that any error in the mapping results in the propagation of error during classification.

A possible criticism is that in these experiments only neural networks were used to perform the classification and that other algorithms may give better results. This may be true, yet the purpose of this research was to see whether or not it was possible to use the mapping scheme to perform CLSA, rather than find the most accurate classifier. Therefore, in the scope of this research, it is more interesting to see the difference between the monolingual and cross-lingual classifiers.

Another important fact that we can gather from these experiments is the importance of vector quality on sentiment analysis classification. Vectors trained on large corpora (GoogleNews vectors), give better results than those trained on smaller corpora (Spanish or Catalan Wikipedia). This seems to corroborate the general trend within the literature which puts emphasis on the use of large datasets for distributional representations. 

A next step would be the addition of parallel data, (e.g., parallel sentences, word alignments from GIZA++ or machine translated data) in order to be able to better create a joint bilingual representation of the data in target and source. \cite{Zou2013} and \cite{Klementiev2012b} provide methods to create bilingual word embeddings while \cite{Zhou2014a} and \cite{Zhou2015a} use deep learning methods for sentence-level, binary CLSA. While these techniques have proven quite effective, many have not yet been evaluated at aspect level nor at a more fine-grained level of sentiment. 



\printbibliography

\end{document}