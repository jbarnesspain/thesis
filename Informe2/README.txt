#################################################################
#								#
#	Experiment on the effects of corpus subjectivity and	#
#	sentiment when creating word embeddings for sentiment 	#
#	analysis						#
#								#
#	Author: Jeremy Barnes					#
#	Date: 22.7.2016						#
#################################################################

1. Introduction

This experiment aims to quantify the amount of subjective and 
sentiment contained in a corpus and examine what effects this
has on creating word embeddings for sentiment analysis. We 
predict that word embeddings trained on corpora with higher 
subjectivity and sentiment content will give better results.

-----------------------------------------------------------

2. Materials

2.1 Opinion Finder
This is a subjectivity and polarity detection system created by
Janyce Wiebe and many other collaborators. It has a high-recall
and high-precision classifier for subjectivity, as well as a
polarity classifier. This system will enable us to quantify the
amount of subjectivity and sentiment in a corpus.

The high-recall classifier has an accuracy of 76%, subjective 
precision of 79%, subjective recall of 76%, and subjective 
F-measure of 77.5% on the MPQA dataset.

The high-precision classifier has around 91.7% subjective 
precision and 30.9% subjective recall. Objective precision is 
83.0% and objective recall is 32.8%.

The polarity classifier has an overall accuracy of 73.4% on the 
MPQA dataset.

2.2 Corpora

2.2.1 Wikipedia Corpus

2016 wikipedia dump which has been cleaned, sentence tokenized,
word tokenized and lower-cased.

2.2.2 Opener Corpus

English Opener Corpus

2.2.3 English Europarl

2.2.4 Multi UN

2.2.5 Open Subtitles

2.2.6 IMDB Sentence Polarity Dataset

-----------------------------------------------------------

3. Setup

3.1 Quantify the subjectivity and sentiment content of each corpus
In order to do this, we ran OpinionFinder on each of the corpora.
This gave results for high-precision subjectivity, high-recall subjectivity
and polarity. In order to quantify the amount of subjectivity, we took 
the number of sentences reported as subjective and divide them by the 
total number of sentences in the corpus. This gave us an idea of
the subjectivity content of one corpus compared to the others, although
it is not a precise measurement. We did the same with high-recall and
polarity to get three scores for each corpus. Like we expected, the wikipedia
corpus has the lowest scores for subjectivity and polarity. This is likely
because of its encyclopedic format. The corpus with the highest
subjectivity and polarity scores is the sentence polarity dataset. 
Also notice that the Europarl dataset is more subjective than the OpeNER
dataset, but less polar. This seems to confirm the soundness of our methodology. 

3.2 Creating the word vectors from each of the corpora
We created 300-dimensional word vectors for each corpus using the 
Skip-gram algorithm with a window of 10 words, and 5 negative samples. 
As a first approximation to the quality of the word vectors, we test them
on the word2vec word analogy task (Mikelov et al., 2013). The results seem to show
that the smaller corpora (OpeNER, Open Subtitles, IMDB) did not capture enough information in order to capture
the offsets required to answer these questions correctly.

3.3 Testing the word vectors
We tested the word vectors on the IMDB Sentence Polarity dataset. 
Following Iyyer et al. (2015), we use a Deep Averaging Network. Unlike
their research, we only want to assess the quality of the word embeddings,
so we do not update them while training. A training example is the average of
the word embeddings for each word in the sentence. This neural bag-of-words
model does not take the order of the words into consideration, but gives 
results similar to more sofisticated approaches.

-----------------------------------------------------------

4 Results


Subjectivity/Polarity scores for each corpus

				 Europarl	Multi UN	OpeNER		Open Sub	Sent Pol	Wikipedia
high recall subjectivity: 	 49.07 %	32.97 %		35.71 %		7.67 %		55.81 %		18.16 %
high precision subjectivity: 	 6.07 %		5.12 %		6.02 %		0.76 %		17.87 %		3.23 %
polarity:			 28.17 %	23.34 %		36.06 %		25.16 %		47.16 %		19.33 %
			

Accuracy of embeddings on word2vec questions

English Europarl: 		35%
Multi UN: 			43.3 %
Opener: 			0%
Open Subtitles: 		10.3%
Sentence Polarity: 		1.1%
Wikipedia: 			75.5%

Accuracy of classifier on Sentence Polarity dataset using embeddings trained on full datasets.

English Europarl: 				69.37%
Multi UN: 					69.68%
Opener: 					69.98%
Open Subtitles: 				65.25%
Sentence Polarity: 				66%
Wikipedia: 					75.69%
Glove Vectors trained on 6B words: 		75%
	


Corpora Statistics:

			sents		tokens		avg. sent length	type-token ratio
English Europarl: 	1,965,734	49,158,635	27.65			
Multi UN: 		11,350,967	285,077,155	28.17			
Opener: 		2065		36,697		17.77		 	10.79%
Open Subtitles: 	506,188		3,191,460
Sentence Polarity: 	10,661		224,014		21.59			8.82%
Wikipedia: 		118,900,197	1,795,681,498


-----------------------------------------------------------

5 Ruling out corpus size

5.1 We decided to test whether a subjective corpus of the same size as the wikipedia corpus
would produce better results. This was most easily acheived by limiting the size of the wikipedia
corpus. We took the first 10 million sentences from Wikipedia, Europarl and Multi UN corpora in
order to create word embeddings. We trained and tested them as before.

5.2 Results

Accuracy of classifier on Sentence Polarity dataset using embeddings as only features

English Europarl: 	67.37%
Multi UN: 		68.12%
Wikipedia: 		72.93%

-----------------------------------------------------------

6 Combining Wikipedia and Subjective corpus

6.1 We wanted test whether adding subjective sentences to our wikipedia corpus would improve the vector representations. In order to do this, we extracted the subjective sentences from the Europarl and Multi UN corpora and created four new corpora: a high precision subjective Europarl, a high recall subjective Europarl, a high precision subjective Multi UN and a high recall subjective Multi UN. We used the first 10 million tokens from Wikipedia and the first 10 million from the extracted sentences from each of these corpora to train a language model using the same word2vec set up as before. Therefore, each set of embeddings is trained on a total of 20 million tokens. The results are shown below.

Size of extracted subjective corpora.

		Europarl high precision	 high recall	Multi UN	high precision		high recall       Wikipedia (20M tok)
sentences		 125,516	 998,124			1,347,141		4,333,673
words			 4,497,180	 30,301,411			43,071,952		135,770,190

accuracy		 73.5% 		 73.125%			73.25%			73.5%		  73.5%	

7. Using only the subjective sentences from Wikipedia

7.1 Finally, we compare using a subset of only subjective wikipedia and compare that with the same amount of mixed wikipedia data. We again take only the first 10 million words from each to train our Word2Vec model.

high_recall		high_precision		normal
72.12%						73.5%

