
\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage[natbib=true, backend=bibtex, style=authoryear,
citestyle=authoryear, isbn=false, url=false, doi=false]{biblatex}
\bibliography{/home/jeremy/NS/Keep/Temp/Thesis/Informe2/Informe2.bib}
\usepackage[]{csquotes}
\renewcommand{\baselinestretch}{1.5}
\usepackage{times}

%opening
\title{Effects of the Subjective and Sentiment Content of a Corpus on Word Embeddings for Sentiment Classification}
\date{September 18, 2016}
\author{Jeremy Barnes}

\begin{document}

\maketitle

\begin{abstract}

The use of word embeddings (Word2Vec, GloVe, etc.) for NLP tasks has become ubiquitous in recent years. However, the corpora used to train these models is just as important as the algorithm itself. In this paper, we examine the effects of the subjectivity and polarity content of a corpus on the creation of word embeddings for sentiment classification. We show that large, general datasets result in more effective word embeddings, although the amount of subjectivity can contribute to improving certain aspects of classification.

\end{abstract}

\section{Introduction}

There have been a number of successful applications of word embeddings for sentiment analysis (\cite{Socher2013b}; \cite{Tang2014}; \cite{Iyyer2015}). The content of the corpora used to train these word embeddings, however, has not been examined in depth. Most research tends toward using larger and larger datasets in order to build high-quality word embeddings, with some studies using corpora on the magnitude of several billion tokens (\cite{Mikolov2013word2vec}; \cite{Pennington2014}). 

This amount of data, however, is not available in all languages, nor is it necessarily an optimal use of available resources. In fact, there is evidence suggesting that tailoring a dataset for a task can achieve better results with less data (CITE).

This experiment aims to quantify the amount of subjective and sentiment contained in a corpus and examine what effects this has on creating word embeddings for sentiment analysis. We predict that word embeddings trained on corpora with higher subjectivity and sentiment content will give better results. 

\section{Materials}
\subsection{Opinion Finder}
This is a system that was designed for subjectivity and polarity detection \citep{opinionFinder}. It has a high-recall and high-precision classifier for subjectivity, as well as a polarity classifier. This system will enable us to quantify the amount of subjectivity and sentiment in a corpus.

The high-recall classifier has an accuracy of 76\%, subjective precision of 79\%, subjective recall of 76\%, and subjective F-measure of 77.5\% on the MPQA dataset.

The high-precision classifier has around 91.7\% subjective precision and 30.9\% subjective recall. Objective precision is 83.0\% and objective recall is 32.8\%.

The polarity classifier has an overall accuracy of 73.4\% on the MPQA dataset.

\subsection{Corpora}
\subsubsection{Wikipedia Corpus}

This corpus was created from a 2016 wikipedia dump which has been cleaned, sentence tokenized, word tokenized and lower-cased. See Table \ref{stats}.

\subsubsection{Opener Corpus}

This corpus is a the English OpeNER sentiment corpus \citep{Agerri2013}. Specifically, we take the subcorpus from the hotel domain. It contains around 2000 sentences, which have been annotated for sentiment. Here, we use it only to validate the subjectivity and polarity quantification methodology. See Table \ref{stats}.

\subsubsection{English Europarl}

The English-Spanish part of the Europarl v7 corpus\footnote{http://www.statmt.org/europarl}	\citep{Koehn2005} is used as parallel data. It is composed of around 2 million aligned sentences from the European Parliament. See Table \ref{stats}.


\subsubsection{MultiUN}

MultiUN is a corpus extracted from the official documents of the United Nations (\cite{multiUn}). It contains around 11 million sentences. See Table \ref{stats}.

\subsubsection{Sentence Polarity Dataset}

This dataset is composed of 5331 positive and 5331 negative snippets taken from movie reviews on the Rotten Tomatoes webpage (\cite{PangLee2005}). This dataset will be used to test the word embeddings for sentiment classification. See Table \ref{stats}.



\begin{table}[h]
\centering
 \begin{tabular}{|c|c|c|c|c|c|} 
 \hline
  					& 	Wiki		& Multi UN 		& OpeNER 	& Europarl 		& Sent. Pol. \\ 
 \hline\hline
 \# Sentences		&  119,793,120		& 11,350,967		& 2065		& 1,965,734		& 10,662	\\ 
 \# Tokens			&  2,075,159,852		& 285,077,155	& 36,697		& 49,158,635		& 224,014	\\
 Avg. Sent Length	&  17.32				& 28.17			& 17.77		& 27.65			& 21.59  	\\ 
 Type-Token Ratio	&  0.5\%				& 0.27\%			& 10.79\%	& 0.26\%			& 8.82\%  	\\ 
 
 \hline
 \end{tabular}
 \caption{Statistics of corpora }
 \label{stats}
\end{table}

\section{Quantifying Subjectivity and Polarity}
The goal of this first experiment was to be able to quantify the amount of subjective and polar information contained in a corpus. In order to do this, we ran OpinionFinder on each of the corpora. This gave results for high-precision subjectivity, high-recall subjectivity and polarity. In order to quantify the amount of subjectivity, we took the number of sentences reported as subjective and divide them by the 
total number of sentences in the corpus. This gave us an idea of the subjectivity content of one corpus compared to the others, although it is not a precise measurement. We did the same with high-recall and polarity to get three scores for each corpus (see Table \ref{subjectivity}). As we expected, the wikipedia corpus has the lowest scores for subjectivity and polarity. This is likely because of its encyclopedic format. The corpus with the highest subjectivity and polarity scores was the Sentence Polarity dataset.  Also notice that the Europarl dataset is more subjective than the OpeNER dataset, but less polar. The fact that the level of subjectivity and polarity coincide with the kind of corpora seems to confirm the soundness of our methodology. 

\begin{table}[h]
\centering
 \begin{tabular}{|c|c|c|c|c|c|} 
 \hline
  					& 	Wiki 	& Multi UN 	& OpeNER 	& Europarl 	& Sent. Pol. \\ 
 \hline\hline
 High recall subj. 	&  18.16\% 		& 32.97\%	& 35.71\%	& 	49.07\%	& 55.81\%	\\ 
 High precision subj.&  3.23\%		& 5.12\%		& 6.02\%		& 	6.07\%	& 17.87\%	\\
 Polarity 			&  19.33\%		& 23.34\%	& 36.06\%	& 	28.17\%	& 47.16\%  	\\ 
 \hline
 \end{tabular}
 \caption{Subjectivity and Polarity Content of Corpora: 
  less subjective to more subjective as you move left to right }
 \label{subjectivity}
\end{table}

\section{Creation and Testing of Word Embeddings}
We created 300-dimensional word vectors for each corpus using the Skip-gram algorithm with a window of 10 words, and 5 negative samples. As a first approximation to the quality of the word vectors, we test them on the word2vec word analogy task (Mikelov et al., 2013). The results seem to show that the smaller corpora (OpeNER, Open Subtitles, Sentence Polarity) did not capture enough information in order to capture the offsets required to answer these questions correctly. Yet there are many reasons to think that this offset testing strategy is not reliable. Therefore, we test them in an extrinsic fashion for the task of sentiment classification.

We tested the word vectors on the Sentence Polarity dataset.  Following the work of Iyyer et al. (2015), we use a Deep Averaging Network to perform sentiment classification. Unlike their research, we only want to assess the quality of the word embeddings, so we do not update while training.

For each sentence in the Sentence Polarity dataset, we create a training example. A training example is the average of the word embeddings for each word in the sentence. If a word is not found in our word embedding model, we leave it out and do not count it in the averaging. This neural bag-of-words model does not take the order of the words into consideration, but has proven to give results similar to more sophisticated approaches.

From the results in Table \ref{full corpora} we can see that the word embeddings created with large corpora outperform the those created with smaller, more subjective corpora.


\begin{table}[h]
\centering
 \begin{tabular}{|c|c|c|c|c|c|c|} 
 \hline
  					& 	Wiki	& Multi UN 	& OpeNER 	& Europarl 	& GoogleNews &	 GloVe \\ 
 \hline\hline
 W2V questions 		& 	75.5\%		& 43.3\%		& 0\% 		& 35\%		&	15\%		&			\\
 Accuracy 			&  75.93\%		& 69.8\%		& 69.98\%	& 67.94\%	&	70.86\%	&	 75\%	\\ 
 Precision  			&  .79			& .69		& 			& .64		& 	.75		&	.86		\\
 Recall				&  .7			& .71		& 			& .78		& 	.63 		&	.57		\\
 F1 Score 			&  .74			& .7			& 			& .71		& 	.68 		&	.68		\\  
 \hline
 \end{tabular}
 \caption{Results of Full Corpora}
 \label{full corpora}
\end{table}


\section{Ruling Out Corpora Size}

We decided to test whether a subjective corpus of the same size as the wikipedia corpus would produce better results. This was most easily achieved by limiting the size of the wikipedia corpus. We took the first 10 million tokens from Wikipedia, Europarl and Multi UN corpora in order to create word embeddings. We trained and tested them as before.


\begin{table}[h]
\centering
 \begin{tabular}{|c|c|c|c|} 
 \hline
  					& 	Wikipedia 	& Multi UN 	& Europarl \\ 
 \hline\hline
 Accuracy 			&  72.93\%		& 69.38\%	& 69.25\%	\\ 
 Precision  			&  .657			& .67		& .76		\\
 Recall				&  .815			& .75		& .56		\\
 F1 Score 			&  .728			& .71		& .64		\\  
 \hline
 \end{tabular}
 \caption{Results of 10M word Corpora}
 \label{table:1}
\end{table}

\section{Combining Wikipedia and Subjective Corpus}
We wanted test whether adding subjective sentences to our wikipedia corpus would improve the vector representations. In order to do this, we extracted the subjective sentences from the Europarl and Multi UN corpora and created four new corpora: a high precision subjective Europarl, a high recall subjective Europarl, a high precision subjective Multi UN and a high recall subjective Multi UN. We used the first 10 million tokens from Wikipedia and the first 10 million from the extracted sentences from each of these corpora to train a language model using the same word2vec set up as before. Therefore, each set of embeddings is trained on a total of 20 million tokens. The results are shown in Table \ref{combined}.

\begin{table}[h]
\centering
 \begin{tabular}{|c|c|c|c|c|c|} 
 \hline
		& 	Europarl prec. 	& Europarl rec. & Multi UN prec. & Multi UN rec. & Wikipedia 20M\\ 
 \hline\hline
 Accuracy 			&  73.5\%		& 73.13\%	& 73.1\% 	&	72.4\%	 	&	73\%	\\ 
 Precision  			&  .79			& .77		& .79	  	&	.71			&	.7		\\
 Recall				&  .64			& .67		& .62		& 	.74			&	.8		\\
 F1 Score 			&  .71			& .72		& .7			&	.73			&	.75		\\  
 \hline
 \end{tabular}
 \caption{Results of Combined Wikipedia and Subjective Corpora }
 \label{combined}
\end{table}

\section{Subjective Wikipedia Corpus}
Finally, we compare using a subset of only subjective wikipedia and compare that with the same amount of mixed Wikipedia data. Here again take only the first 10 million words from each to train our word2vec model. The results in Table 	\ref{extracted wikipedia} suggest that the extracted corpora lose in recall, while gaining some precision for sentiment classification. 

\begin{table}[h]
\centering
 \begin{tabular}{|c|c|c|c|} 
 \hline
		& 	High prec. 	& High rec. & Wikipedia \\ 
 \hline\hline
 Accuracy 			&  71.4\%		& 70.87\%	& 73\% 	\\ 
 Precision  			&  .77			& .75		& .7		\\
 Recall				&  .61			& .63		& .8		\\
 F1 Score 			&  .68			& .68		& .75	\\  
 \hline
 \end{tabular}
 \caption{Results of Extracted Subjective Wikipedia Corpora }
 \label{extracted wikipedia}
\end{table}

\section{Lexical Overlap between extracted subjectivity training corpus and sentence polarity test set}

After seeing the drop in recall, we tested the overlap in vocabulary between the extracted subjectivity corpora and the sentence polarity corpus. Our hypothesis was that the extracted corpora lost in recall because they lacked some of the vocabulary from the test set. In order to test this, we examined the overlap in vocabulary. Let $V_{pol}$ be the set of words in the the sentence polarity corpus and $ V_{k}$
be the set of words in each extracted corpus $k$. 


\begin{align}
Lexical Overlap = \dfrac{|V_{pol}| \cap |V_{k}|}{|V_{pol}|} 
\end{align}
 
If the lexical overlap of the sentence polarity test set and the training corpus is small, this will negatively affect the results of the classifier, as it will lack necessary information. We also test for lexical diversity of the extracted subjectivity training corpora. We use the Type/Token ratio (\textbf{TTR}) as a measure. Let $V_{k}$ = Vocabulary and $T_{k}$ = the tokens of corpus $k$. 

\begin{align}
Type Token Ratio = \dfrac{|V_{k}|}{|T_k|}
\end{align}

This measurement is not reliable on different text sizes. Therefore, we take the first 100,000 tokens from each corpus to produce the TTR. The results are shown in Table \ref{Overlap}.
 
\begin{table}[h]
\centering
 \begin{tabular}{|c|c|c|c|c|c|c|c|} 
 \hline
  				&   Wiki & Wiki P & Wiki R & MulUN P & MulUN R 	& Euro P & Euro R 		 \\ 
 \hline\hline
 \# tokens		  &	1.8B\~{}& 	6.5M	\~{}	& 21M\~{}	 & 43M\~{}&	136M\~{}	& 45M\~{}	&30M\~{}\\
 Lexical Diversity & .128  &	.153		& .136	 &  	.079		& 	.075		& .079	  	&	.072 	\\ 
 Overlap Train	  & \textbf{96.89}\% &	84\%		& 87.98\%& 64.5\%	& 81.05\%	& 68.45\% 	&	78.90\%\\
 Overlap Test	  &  \textbf{98.26}\%&	90.5\%	& 92.74\%& 74.35\%	& 81.05\%	& 68.45\% 	&	78.90\%\\
 Accuracy		  & \textbf{75.5}\%	&	71.4\%	& 70.87\%& 73.2\%	& 72.4\%		& 73.5\%		&	73.13\%	\\
 Precision		  &\textbf{.79}	&     .77		& .75	 & \textbf{.79}	& .71	& \textbf{.79}&.77 \\
 Recall			  &	.7 &	.61		& .63	 & .62		& \textbf{.74}		& .64		&   .67 \\
 F1				  &\textbf{.74}	&	.68		& .68	 & .7		& .73		& .71		&   .72 \\
  
 \hline
 \end{tabular}
 \caption{Lexical diversity scores and overlap with test vocabulary in each training corpus. The \textit{P} after a corpus denotes it was created using the high-precision algorithm in OpinionFinder, while the \textit{R} was created with the high-recall algorithm.}
 \label{Overlap}
\end{table}

As we can see in Table \ref{Overlap}, the number of words that do not have a vector representation in the full wikipedia model is 3.11\% for the training section and 1.74\% for the test section of the sentence polarity dataset. Compare this to the 10-31\% of missing vectors from the extracted datasets. Obviously, this creates a sparseness of data during both training and testing.

\subsection{Types of missing words}
After seeing the number of words which were missing, we decided to study these qualitatively. We collected a list of the words that appear in the sentence polarity dataset which were not found in the the embeddings trained on the full wikipedia (NFIwiki) and of those not found in the extracted MultiUN corpus (NFIMultiUN). 

\begin{table}[h]
\centering
 \begin{tabular}{|c|c|c|c|c|c|} 
 \hline
  			& 	nouns 		& adjs		& verbs 			& adverbs 	& total \\ 
 \hline\hline
 NFIwiki 	&  41.6\% 		& 22.56\%	& 18	\%			& 	15.97\%	& 288 words	\\ 
 NFIMultiUN&  42.9\%		& 22.7\%		& 18.45\%		& 	14.4\%	& 2070 words	\\
 \hline
 \end{tabular}
 \caption{Words not found in embeddings trained on full wikipedia (NFIwiki) and the extracted MultiUN corpus P (NFIMultiUN) }
 \label{Types of missing words}
\end{table}

A revision of each part of speech category revealed that the nouns not found in the embeddings models were often misspellings of more common nouns (\textit{fantasi, alientation, baaaaaaaad}) or creative use of language (\textit{vidgame, dateflick, splatterfests, artsploitation, witlessness, drippiness, naturedness, diciness, gooeyness}), both of which are important for categorization.

Even more important are the adjectives (\textit{surfacy, disposable, nonchallenging, unrecommendable, unsuspenseful, uncinematic, unslick, unfakeable, snazzy}) and adverbs (\textit{heartwarmingly, bottomlessly, hammly, dullingly, repellently, forgettably, uncharismatically, appallingly}) which are not found in these models. These are words that often carry the most sentiment information in a sentence. However, in corpora such as Wikipedia or MultiUN, we cannot find enough examples to create useful vector representations of such words.

\section{Backoff Model}
In order to counteract the negative effects that missing vocabulary caused on the recall of extracted corpora models, we created a simple backoff model. When constructing the dataset, if a word is not found in the extracted corpus model, we take it from the full wikipedia model. This should improve the recall, as there will be far fewer words with a generic/no vector representation. As we can see in Table \ref{Backoff models}, the recall on 3 of the 4 datasets has improved significantly.

\begin{table}[h]
\centering
 \begin{tabular}{|c|c|c|c|c|} 
 \hline
		& 	Europarl prec. 	& Europarl rec. 		& Multi UN prec.		& Multi UN rec. \\ 
 \hline\hline
 Accuracy 			&  71.9\%		& \textbf{73.33}\% 	& 72.4\% 			&	73.25\%	 	\\ 
 Precision  			&  \textbf{.79}	& .72		& .68	  			&	.69			\\
 Recall				&  .59			& .78		& \textbf{.87}		& 	.84			\\
 F1 Score 			&  .68			& .74		& \textbf{.76}		&	\textbf{.76}			\\  
 \hline
 \end{tabular}
 \caption{Performance of backoff models }
 \label{Backoff models}
\end{table}


\section{Concatenation}

Although the recall improved using the back-off method, the precision scores were negatively affected. This is likely due to the fact that the vectors from the extracted corpora and those from the wikipedia corpus do not lie in the same vector space. Therefore, when there is a vocabulary word missing from the extracted vector space model, adding information from the wikipedia model isn't necessarily useful. The sentiment classifier cannot make use of these differing representations.

For these reasons, we decided to concatenate the sentence vector created with the wikipedia corpus and the one created with each extracted corpus. This gave us a 600-dimensional vector with which to train a sentiment classifier. Because there exists the possibility that larger vectors by themselves could improve accuracy, we compared these concatenated vectors to a 600-dimensional vector space model trained on the full wikipedia. If the 600-dimensional wikipedia model gave better results than the concatenated vectors, this would indicate that the size of the vectors is the most important variable. In Table \ref{concat models} we can see that the concatenated vectors perform better than the wikipedia vectors.
\begin{table}[h]
\centering
 \begin{tabular}{|c|c|c|c|c|c|} 
 \hline
 Sent. Pol  & wikipedia & 	Europarl prec. 	& Europarl rec. 		& Multi UN prec.		& Multi UN rec. \\ 
 \hline
 Accuracy 	& 74\%	&  76\%		& 76\% 		& 76.5\% 			&	75\%	 	\\ 
 Precision  	& .74	&  .76		& .76		& .77	  			&	.76			\\
 Recall		& .74	&  .76		& .76		& .76				& 	.76			\\
 F1 Score 	& .74	&  .76		& .76		& .76				&	.76		\\  
 \hline\hline
 OpeNER & & & & &\\
 \hline
 Accuracy 	& \%	&  70.84\%		& 70.3\% 		& 69.94\% 			&	71.37\%	 	\\ 
 Precision  	& .	&  .697		& .689		& .676	  			&	.688			\\
 Recall		& .	&  .642		& .642		& .618				& 	.649			\\
 F1 Score 	& .	&  .664		& .662		& .64				&	.665	\\   
 \hline
 \end{tabular}
 \caption{Performance of concatenation models }
 \label{concat models}
\end{table}

\section{Discussion and Further Research}

From the results of all of these experiments, we can come to some conclusions. The general trend to use as much data as possible to create word embeddings seems to have some basis for sentiment analysis. The largest datasets consistently gave the best results. It is worth noting, however, that corpora which are several times smaller still preform well.

Secondly, concatenating the general and subjectivity-extracted vectors gives the best results. This avoids the problem of incorporating information from different vector spaces, since the two spaces are still separated. In this way, a classifier is able to make use of both the more general wikipedia information (the first 300 dimensions), as well as the subjectivity-specific information (the last 300 dimensions).

One interesting fact that we can observe from the results is that using OpinionFinder to extract high precision subjectivity versions of a corpus for creating word embeddings leads to a higher precision score during sentiment classification. This may be due to the fact that the word embedding algorithm is given more relevant information for terms that are found in the test corpus. 


\printbibliography

\end{document}