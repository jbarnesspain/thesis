\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[natbib=true, backend=biber, style=authoryear, isbn=false, url=false, doi=false]{biblatex}
\usepackage[]{csquotes}
\addbibresource{ThesisProposal.bib}
\renewcommand{\baselinestretch}{1.5}
\usepackage{times}

%opening
\title{Distributional Semantics and Neural Networks for Cross-lingual Sentiment Analysis}

\author{Jeremy Barnes}

\begin{document}

\maketitle

\begin{abstract}
Cross-lingual sentiment analysis is an area of research that deals with the leveraging of resources from one language (most often English) to create or improve sentiment analysis tools in a target language. Researchers have mainly concentrated on using Statistical Machine Translation (SMT) as a way to bridge the gap between two languages. However, there are still many situations in which SMT is not the answer.


While SMT has improved a great deal over the years, its use in Sentiment Analysis systems increases the amount of noise. There are also a number of hurdles that even an improvement in SMT can not alleviate. This motivates the need to look for alternate methods to perform Cross-lingual Sentiment Analysis. Specifically, distributional semantic representations show promise.
\end{abstract}

\tableofcontents
\section{Introduction}
Sentiment analysis seeks to define the underlying sentiment of a text. It is closely related to emotion detection, but differs mainly in scope. Sentiment analysis does not attempt to classify the full array of human emotions or even a real subset of them, but rather tends to concentrate on polarity. Polarity in this context is how positive or negative an utterance is. This classification can be performed at varying levels of granularity, depending on the kind of information that we desire.

The best results in sentiment analysis (SA) require the use of a large number of resources, from tokenizers and parsers to large sentiment lexicons or hand-annotated corpora. These all require time, effort and a considerable monetary investment in order to ensure the quality and subsequent usefulness. Therefore, finding a way to perform sentiment analysis in languages which do not have the same amount of resources with minimum effort is an interesting endeavor. This task, known as Cross-Lingual Sentiment Classification (CLSC), has not been given the same attention that other sentiment related tasks have. Yet it would be of great interest to many companies and government organizations which wish to gather information on the public opinions of their products or policies. 

Most research in CLSC has used Statistical Machine Translation (SMT) as a way of bridging the gap between languages, yet there are a number of drawbacks to this: First, some kind of SMT system must be available for the language combination at hand. This, again, requires a great deal of development and the quality of the sentiment analysis system used afterwards depends heavily on the quality of the SMT system. Secondly, several studies show that even high quality SMT introduces a significant amount of noise into the data. Finally, some studies show that there are also mistakes in sentiment that occur in cross-lingual settings which cannot be remedied through improved STM alone, such as certain sentiment items related to culture.

For this reason, a more abstract representation of words and phrases, such as a distributional representation may enable a CLSC system to classify sentences or phrases in a target language (Spanish, Catalan, Basque) while leveraging information from larger resources available in other languages (English).

The use of different kinds of neural networks for SA has shown that they can be very effective. They allow for promising ways of incorporating syntactic and semantic information formerly unavailable to SA, such as compositionality and non-local dependencies. As they are among the most powerful learning algorithms available, they are naturally attractive. Nonetheless, they also have a number of disadvantages: They require large amounts of data, are prone to overfitting and are computationally expensive. For these reasons, there has been little research in SA with neural networks until recently. Thanks to several advances in the architecture and learning algorithms of neural networks, they are now state-of-the-art in many SA tasks.

In order to create competitive CLSC systems, more research is needed on how to use neural representations in order to leverage English data. I propose that competitive CLSC systems can be created by first finding a cross-linguistic mapping between distributional representations of the two languages and then using resources available in English to perform sentiment analysis in the target language.

\section{Review of the Literature}
\subsection{Sentiment Analysis}
Sentiment analysis has been a subject of research for many years now and due to this, there are a number of different approaches that researchers have used in experiments. 

Knowledge-based systems were the first to be proposed, but these required the creation of hand-crafted sentiment lexicons such as the General Inquirer in combination with a number of heuristic rules. While this approach often gives good results and is quite domain-independent, it requires a great deal of specialized knowledge and time, which hinders its usefulness for CLSC.

Since the early 2000s,however, machine learning approaches have gained popularity due to their faster implementation and lower cost. \cite{pang2002thumbs} and \cite{Turney2002} were among the first to show that machine learning techniques could be used for sentiment analysis. Despite the large number of articles published and the amount of research carried out on sentiment analysis, there are still many areas that require further research. 

It is sometimes difficult to define the state-of-the-art in sentiment analysis as the amount of publications and research on the subject is quite large. It is also complicated by the fact that much of the research is conducted on tasks that are not directly comparable. The recent SemEval tasks have begun to provide a higher level of standardization, but there is still a need to progress in this direction. As the situation stands, until quite recently most state-of-the-art sentiment analysis systems, mainly based on feature engineering with support vector machines, (SVMs) achieved nearly 80 percent on the binary polarity classification task.

As the situation stands, until quite recently most state-of-the-art sentiment analysis systems, mainly based on feature engineering with support vector machines, (SVMs) achieved close to 80 percent accuracy on the binary polarity classification task at sentence level. While a full survey of the state of the art in all tasks is beyond the scope of this thesis proposal, it is worth noting that the state of the art in binary polarity classification is now 88.1 percent accuracy \big{(Kim 2014)???} and the fine-grained classification is 51 percent accuracy \cite{Tai2015a}.

Despite the large number of articles published and the amount of research carried out on sentiment analysis, there are still many areas that require further research. One of the main problems of the state of SA is that most research and resources are only available in English. This situation means that cross-lingual sentiment analysis is an interesting and promising research line.

\subsection{Cross-lingual Sentiment Analysis}
Most research in sentiment analysis has traditionally been carried out in English, which means that in languages other than English there are very few resources available.  Therefore, the ability to leverage these resources that already exist in English to perform sentiment analysis in other languages would be a great advantage, as it could in theory increase the accuracy of SA systems built on the small amount of data available in languages that have few resources, or enable the creation of SA systems in languages that have none.

\cite{Mihalcea2007c} explored the automatic creation of Romanian subjectivity tools given English resources. They performed three experiments to test their hypotheses. First, they used an English subjectivity lexicon and a bilingual dictionary to create a Romanian subjectivity lexicon. Next, they  manually create  a parallel corpus and automatically label the English side for subjectivity. These labels are then projected onto the Romanian side of the corpus. Finally, they train a Naive Bayes classifier on the Romanian corpus. Their classifier proved quite accurate (F1=67.85) given that no language-specific Romanian data was used.

Improvements in Statistical Machine Translation (SMT) have proven very useful for cross-lingual sentiment analysis more recently.  SMT systems have been used in a variety of ways to create new resources in a language from already existing English resources. Indeed, the most common use of SMT in cross-lingual sentiment analysis has been to translate some source-language data into English and then use a state-of-the-art English system to perform sentiment analysis. 

\cite{Banea2008d} explored the possibility of using English corpora which had previously been annotated for subjectivity in order to create subjectivity analysis tools for Romanian and Spanish. They performed three experiments, given different assumptions. Their first experiment assumes the existence of a corpus annotated for subjectivity in English. They used SMT to create a Romanian version of the MPQA corpus \cite{Wiebe2005b} and projected the annotations from English. The second experiment assumes only the existence of a subjectivity analysis tool in English. Here they automatically annotated an English corpus with the OpinionFinder tool \cite{Wiebe2005e} and then used SMT and projection of the labels to create their Romanian version. Finally, they perform an experiment that is very similar to the second, but they reverse the direction of translation. They use SMT to translate a Romanian corpus into English and then use OpinionFinder to annotate the translated English corpus. Finally, they project the labels back on the original Romanian corpus. The final F1 score for their experiments (F1=66.07, 69.44, and 67.87 respectively) was only slightly lower than the upper bound of 71.83 which was achieved with monolingual data. This suggested that SMT is a viable alternative to having parallel corpora. 

\cite{Wan2009c} proposed a co-training approach which also made use of machine translation to perform cross-lingual sentiment analysis (positive and negative) on Chinese product reviews. In this article, classification was performed at document level where each document was a complete review. In this paper Wan automatically translates labeled English reviews to Chinese and a Chinese test set and additional Chinese reviews into English. The idea is to create two separate classifiers and uses a bootstrapping method to create additional training material from unlabeled data. Specifically, Wan uses the most confident labels from each classifier to create more training instances for the other classifier. His final classifier reaches an accuracy of 81 percent. 

\cite{Balahur2014d} seem to confirm that machine translation has reached a level of maturity that allows its use for sentiment analysis. They perform a series of more thorough experiments which look at the effects of STM in detail. In fact, they compare the use of different SMT systems (Google, Bing, and Moses) and how they compare to the use of hand-corrected translations from Yahoo translator. The results show that distributional representations, in this case tf-idf, are more robust when translation is poor and that the overall performance of these classifiers are relatively close to the original English classifier. The languages that were involved in this experiment, however, are languages which have high-quality SMT systems. \cite{Popat2013} and \cite{Balamurali2012} indicate that SMT does not work as well for languages that have lower-quality SMT systems.

One might think that if the main problem is the lack of resources in the target language, a logical solution would be to translate a large amount of labeled examples from the source to the target language. However, \cite{Mohammad2015b} point out that simply adding more training data translated from English does not necessarily lead to improvements. In their experiment, they created an Arabic sentiment analysis system and try to improve its performance by using available English-language resources. Their monolingual baseline using n-grams and style features reached an accuracy of 61.98. They then automatically translated an annotated English sentiment analysis corpus to Arabic and used this as extra training data. This system, however, suffered a large drop in accuracy (42.78 \%). They theorize that this happened due to the fact that the sentiment cues learned from the machine-translated training examples do not help the machine learning system if it is then used on natural language. This means that simply having a large amount of data available in English is not enough in itself to achieve significant gains.

\cite{Mohammad2015b} also studied in depth how translation, both SMT and manual, alters sentiment. They found that both kinds of translation often lead to a loss of sentiment; that is, positive and negative sentiment terms have a tendency to be either removed or translated as neutral variants of the original term. This means that way of preserving the original sentiment after translation or translating based on sentiment could improve the overall results.

Both \cite{Balahur2014d} and \cite{Mohammad2015b} point out that the use of SMT invariably leads to noise that affects the machine learning algorithm negatively. According to \cite{Mohammad2015b}, both SMT and manual translation often lead to a loss of sentiment; that is, positive and negative sentiment terms have a tendency to be either removed or translated as neutral variants of the original term. This means that find a way of preserving sentiment after translation or translating based on sentiment could improve the overall results.

Therefore, in order to leverage the useful information found in English data in a more productive way, we must find new techniques which do not alter sentiment cues that are present in natural language. 

\subsection{Neural Distributional Representation of Words}
N-grams have been the most common feature used in sentiment analysis for many years. However, they have many significant drawbacks, including their inability to convey non-local dependencies between words and the curse of dimensionality. This has motivated the recent shift from n-gram based methods towards distributional representations in Sentiment Analysis. Although there is a long history of vector space methods in computational linguistics and NLP (Latent Semantic Analysis, Latent Dirichlet Allocation, tf-idf, etc.), word embedding methods following the approach set forth by \cite{Bengio2003}, such as \textit{word2vec} \cite{Mikolov2013word2vec} and GloVe \cite{Pennington2014}, have made progress in several aspects.

These methods differ greatly from n-gram models in several ways. First, they include information about words that occur after the center word, whereas n-gram models only include information about words that occur earlier. This gives a greater amount of information about the distributional characteristics of a word.

According to \cite{Klementiev2012b}:

The goal of statistical language modeling methods is to estimate the joint probability distribution of word sequences occurring in a natural language. Neural probabilistic models learn a latent multi-dimensional representation of words and use them to estimate the probability distribution of word sequences. An important side-effect of training neural language models is the fact that the learned latent representations capture syntactic and semantic properties of context words, because these properties are predictive of a possible next word.

It is precisely the ability to capture syntactic and semantic information that make these word embeddings interesting for sentiment analysis.

%[Explicitly comment that this is one of 5] Mikolov et al. (2013)a – 

\cite{Mikolov2013word2vec} proposed two methods of learning neural ‘word embeddings’: the first, they called the Continuous Bag-Of-Words (CBOW) model and the second the Continuous Skip-gram Model. The CBOW model is a log-linear model, similar to a feedforward neural net model, but without the hidden layer. The optimization objective is to predict a center word given a context window of x words. 

The Continuous Skip-gram model is similar, but instead of predicting a center word given the context, the idea is to predict the context given a center word. 

Although the Skip-gram model has proven to give more accurate results on many tasks, \cite{Mikolov2013word2vec} recommend using the CBOW model, as it is computationally cheaper and can therefore be more easily trained on much larger datasets. In practice, the ability to use larger corpora seems to outweigh the benefits given by the Skip-gram model.

The resulting vectors of both of these models appear to contain both semantic and syntactic information about their corresponding words. An interesting property of these word embeddings is the ability to do simple algebraic operations in order to test them. In order to compute the vector for ‘smallest’, it is possible to compute X = vector(‘biggest’) - vector(‘big’) + vector(‘small’) and do a search of the vector space to find the nearest vector to X measured in cosine distance. Similar operations can be used to find more semantic differences. This seems to indicate that these neural predictive models do contain this information at some level. \cite{Baroni2014b} performed a thorough comparison of neural word embeddings and traditional count-based distributional representations on a number of semantic and syntactic tasks and found that the neural models consistently outperformed the count-based models and were even state-of-the-art in several of the tasks. 

\cite{Pennington2014} also propose a method to create vector space representations of words, which they call GloVe (Global Vectors). They point out that the two current approaches to vector space models each have certain flaws. Namely, matrix factorization methods (LSA, HAL, etc.) properly account for co-occurence counts throughout the document, but do not do well on analogy tasks. Shallow window methods (CBOW, Skip-gram), on the other hand, do well on analogy tasks but fail to take advantage of the statistics available in the data.

They find that in these vector representations, the most significant information is found in the ratio of probabilities between the counts rather in the raw probability counts themselves. For this reason they propose a representational model that tries to combine both global and local statistic, in order to benefit from both. After evaluating their model on the analogy test set (see \cite{Mikolov2013word2vec}) they also evaluated the results of several other NLP tasks in order to corroborate the fact that these word vectors improve the performance of other systems. They concluded that these word vectors do indeed improve performance.

Other research in distributional representations of words has also indicated that many of the vectorial relationships that exist between words projected into vector space are comparable across different languages. \cite{Mikolov2013translation} propose a way to exploit the similarities between languages in such a distributional representation. They first create two monolingual language models with large corpora and then use a bilingual dictionary to find a linear projection between the two. 

This linear mapping allowed the authors to fill gaps in phrase tables used in machine translation and therefore increase the precision and coverage of their system. Despite the simplicity of their mapping scheme, it proved to be useful for Machine Translation. This strategy could prove valuable to research in Cross-Linguistic Sentiment Analysis as well, providing a way to increase the coverage of such system by filling gaps in sentiment lexicons or by allowing machine learning systems to use information from large English corpora which have been annotated for sentiment. Indeed, it would not be unthinkable to create such a linear projection from English to Catalan or Basque, making it possible to create CLSC systems from relatively few resources. 

Despite these many examples, there have been very few attempts to use this approach for CLSC, and as far as the author is aware, none at aspect-level.


\subsection{Deep Learning}
In order to make use of these word embeddings, a researcher has a great number of machine learning algorithms available. Naive Bayes and SVMs have traditionally been the common choice for NLP research but recently Neural Networks have regained popularity. Neural Networks are potentially one of the most powerful learning algorithms and certain developments in their implementation, such as efficient backpropagation and the discovery of ways to avoid exploding or disappearing gradients, have yielded state-of-the-art results in many different areas of research, from image recognition to natural language processing.

The simplest neural network is defined as such:
Given a set of labeled training examples \(\{x_i, y_i\}\), a neural network would be defined as:

\[h = W(x)+b\] 

where \textit{W} are the weights between the layers and \textit{b} are the bias units associated with each layer. 

Deep Neural Networks are simply the stacking of various levels of Neural Networks and can have a variety of architectures (Convolutional NNs, Recurrent NNs, Recursive NNs, Long Short-Term Memory NNs, etc), each of which are more suited to certain tasks and certain data. The main advantage of these structures applied to natural language is that they are able to learn differing levels of abstraction. 

%[Explicitly comment that this is one of 5]
\cite{Socher2013b} presents a novel approach to sentiment analysis, as well as introducing a new corpus designed specifically for fine-grained sentiment analysis; The Stanford Sentiment Treebank. The 215,154 phrases were taken from a subsection of the original IMDB corpus used by \cite{pang2002thumbs}. The sentences were annotated for five degrees of polarity (strong negative, negative, neutral, positive, strong positive) at each node of a binary parse tree. This enables the training of deep neural networks that are able to predict the polarity of a sentence at each node of the parse tree, rather than at sentence or even aspect level.

\cite{Socher2013b} then introduce a new model, which they call the Recursive Neural Tensor Network (RNTN). This model takes as input parsed sentences and uses a tensor product to model composition. This is especially useful for sentiment analysis, as it gives us a way to represent the semantic composition of modifier + adjective, negation and contrastive conjunction, which have traditionally requires heuristic rules that involve the flipping of sentiment or complicated weighting schemes. Indeed, Socher et al. give several examples of how this model correctly handles contrastive conjunction, high-level negation, and the difference between negating positive and negative sentences.

\cite{Tai2015a} used Tree-structured Long Short-Term Memory (LSTM) neural networks for sentiment analysis. These neural networks incorporate a memory cell that allows the learning algorithm to remember important information over time, thus increasing its ability to compute non-local dependencies. They showed that this architecture is state-of-the-art in fine-grained SA.

These approaches have led to a significant improvement in the state of the art, as well as indicating a promising new line of research within sentiment analysis, pushing accuracy from around 80 percent to nearly 90 percent on the binary task. These improvements, however, are for the moment restricted to English, as all of the datasets necessary for these approaches are only found in English. This motivates the need to find a method to transfer this success to other languages.

The work that most closely resembles my proposal is that of \cite{Zhou2015a}. Here the authors make use of neural networks to perform Cross-Lingual Sentiment Analysis. They try a technique in which Large parallel corpora are used to create a bilingual distributional semantic representation. In order to do this, they experiment with autoencoders.  The objective of an autoencoder is to produce a higher-level representation of a document given a bag-of-words representation and then recreate the bag-of-words representation as close as possible. During the encoding phase, \cite{Zhou2015a} pretrain the sentences in each language using denoising autoencoders. They then train a bilingual autoencoder on these pretrained sentences. The objective is to be able to reconstruct the original sentence in either language, thus generating a bilingual representation of any sentence in either language. 

They then use the weights learned during training as high-level latent representation of the documents. They can then transform any document into the representation \textit{y(d)} by repeating the procedure and then train an SVM on the labeled bilingual encoding data. Finally, they run a series of experiments which demonstrate that this method is state-of-the-art in several tasks.

This method, however, assumes the existence of large parallel corpora between the target and source language. While this may exist between Chinese and English, for many language combinations it is not available. Therefore, being able to find a bilingual representation without large parallel corpora is also necessary.

\subsection{Aspect-level Sentiment Analysis}

Aspect-level sentiment analysis, also known as opinion-mining is another aspect that has not yet been dealt with thoroughly in cross-lingual contexts. Most CLSC has been performed only at sentence or document level. However, there are many contexts in which aspect-level CLSC would be more beneficial.

\cite{Liu2015} defines the an opinion as a quadruple:

\[(g, s, h, t)\]

where \textit{g} is the sentiment target, \textit{s} is the sentiment of the opinion about the target \textit{g}, \textit{h} is the opinion holder and \textit{t} is the time when the opinion is expressed. According to Bing, all four components are essential and the lack of one or more can lead to difficulties in the analysis of sentiment. 

There are two main approaches that are most common in the literature. First, there is the supervised machine learning approach, which uses either manually engineered features or automatically induces these in order to correctly predict the sentiment of an opinion target. Second, there is the lexicon-based approach, which uses sentiment lexicons and heuristic rules to perform this prediction.

In CLSC there has been very little research at this level of granularity. \cite{Wan2009c} used English and Chinese reviews to perform CLSC, but the review was taken as a full document and aspects were not differentiated. \cite{Lambert2015a} used constrained SMT to automatically translate aspects and opinions while preserving the boundaries of these. \cite{Lin2014a} deal with aspect-level SA, but concentrate on extracting aspects.

\section{Objectives}

My objectives are the following: First, I intend to examine the effects of using different mapping strategies in order to create a mapping between two or more monolingual neural word embedding models for sentiment analysis. The ability to connect two such models in a meaningful way is crucial to the further approaches. There are several approaches to this problem already in the literature (Mikolov et al., 2013b, Zou et al., 2013), yet neither have been tested on sentiment analysis. Second, I intend to propose new methods of creating sentiment analysis systems leveraging the information available with such mappings. Finally, I intend to propose new ways of creating sentiment labelled corpora and lexicons in a target language, using this mapping strategy.

\subsection{General Objectives}
\begin{enumerate}
\item Use Neural representations across language barriers to conduct aspect -level Sentiment Analysis. 
\item Create sentiment lexicons that can be used cross-linguistically.
\end{enumerate}


\subsection{Specific Objectives}
\begin{enumerate}
\item Create a bilingual mapping between neural representations of  target (Catalan, Basque) and source language (English).
\item Use this bilingual mapping to create a Sentiment Analysis system in the target languages.
\item Determine how much data is necessary to perform these operations.
\item Determine how ambiguity can be resolved in these models.
\item Determine the effects of domain on this mapping, i.e. is it necessary for all data to be in-domain?
\item Evaluate the performance of these systems at aspect-level.
\item Create bilingual sentiment lexicons composed of neural representations and their mapping.
\item Use English corpora labeled for fine-grained sentiment to perform SA in Catalan/Basque. 

\end{enumerate}



\section{Theoretical Framework}

\subsection{Empirical NLP}

\subsection{Machine Learning}
Most research done in SA involves the use of machine learning. This approach allows for the creation of Sentiment Analysis applications which require less time and money than symbolic approaches. Typically an annotated corpus is used as the gold standard. The creation of this gold standard corpus is crucial to the success of the machine learning system. Once it has been compiled, a machine learning algorithm may be used to learn the probability distribution of the labels given some features. Feature engineering is one of the most important aspects of the creation of a sentiment analysis system. Choosing which features to use and how to represent them conditions the rest of the research. 

\section{Hypotheses}

\subitem
\begin{enumerate}
\item Monolingual word-embeddings (neural word vector representations) can be linked so that their semantic spaces are projected in the same way in a vector space representation. This can enable the training of a classifier which uses these word embeddings as features to classify sentiment in the target language.
\item For languages which have few or no available parallel corpora with English, this approach will provide a method to classify sentiment that is comparable to monolingual methods.
\item For languages which have few high-quality SMT resources, this technique will prove more accurate.
\end{enumerate}

\section{Research Methodology}
\subsection{Adopted Methodology}
The research will proceed in three major phases. During the first phase I will collect the tools needed to conduct the experiments. This will involve the collection of corpora for the training of the machine learning algorithms, the exploration of ways to represent the metadata that will be incorporated in the corpora (either as text files or xml formats) and the tools necessary to process the data (tokenizers, POS taggers, parsers, etc). Machine learning toolkits are also necessary. A list of tools and resources that I will need follows:

\subparagraph{Tools}

\begin{enumerate}
\item OpeNER corpora
\subitem
This is a set of corpora of hotel reviews available in several languages compiled in the framework of the OpeNER project (reference). The reviews have been tokenized, POS-tagged, lemmatized and parsed. They also have annotations for aspect-level sentiment: specifically, opinion holders, opinion targets and opinion phrases have been annotated. The use of these corpora will allow me to compare the results from my CLSC systems on aspect-level tasks with the results from state of the art monolingual SA systems in languages with larger resources (English, Spanish, German)

\item Catalan/Basque Booking Corpora
\subitem 
	These two corpora have been created by myself with several members of GLiCom in order to evaluate the use of CLSC on languages with fewer resources and with less presence in the literature. They contain reviews of hotels that were crawled from Booking.com between September and December 2015. These reviews were then cleaned and next tokenized and pos-tagged (using Freeling for the Catalan corpus and Ixapipes for the Basque corpus). Finally, the reviews were annotated following the procedure used to create the OpeNER corpora in order to allow the comparison of results.

\item Wikipedia Corpora
	\subitem
	These corpora in English, Catalan, and Basque were collected from Wikipedia dumps in January 2016. They were preprocessed using a python script created by Giuseppe Attardi et al. in the Tanl research group at the University of Pisa. They were then sentence tokenized. These corpora will be used to create the word embeddings.

\item KAF/NAF representation
	\subitem 
	This is a standoff XML annotation scheme used in the Kyoto project and OpeNER project respectively (\cite{Bosma2009}, \cite{Fokkens2014a}). The main benefit that this annotation scheme has is that it allows for many layers of annotation without the need to change the original text. Since this is the annotation scheme used in the OpeNER project, I will use it to annotate the Basque and Catalan corpora, in order to improve compatibility of the datasets and therefore comparability of the results.
	
\item Freeling 2.1
	\subitem
	Freeling 2.1 is a free NLP toolkit created in the TALP research group at the Universitat Politècnica de Catalunya \cite{padro10b}. It incorporates the normal NLP resources, such as a tokenizer, morphological analyzer, POS-tagger and parsers, for Spanish and Catalan, amongst other languages. I will use it to process the text in the Catalan reviews.
\item Ixapipes
	\subitem
	Similar to Freeling but for Basque.
	
\item NLTK
	\subitem
	NLTK is an NLP library that includes a wide range of tools and resources. I will use this to tokenize the English wikipedia corpus.
	
\item TensorFlow
	\subitem
	This is a DeepLearning toolkit made available by Google. It allows for a relatively simple creation of Neural Networks in python. I will use it to perform experiments in all languages.
	
\item Gensim toolkit
	\subitem
	This toolkit implements the neural language model algorithms, \textit{word2vec}, from \cite{Mikolov2013word2vec} in python. It was created by Radim Rehurek \cite{rehurek_lrec}. These word embeddings are the basis of the CLSC systems that I propose.
\item Glove toolkit
	\subitem
	A similar toolkit to \textit{word2vec} from \cite{Pennington2014}. It will be used to create word embeddings.
\item Python
	\subitem
	This programming language will be used to implement the learning algorithms and create the CLSA systems.

\end{enumerate}

	

During the second stage of research, I will conduct a series of experiments to determine which mapping strategy is most useful for CLSC. First I will map monolingual language models through the use of a bilingual dictionary following Mikolov et al. (2013)b. Concretely, I will create two monolingual neural representations, one in the source language, \textit{S} and one in the target language, \textit{T}. Following \cite{Mikolov2013translation}, suppose that we are given a set of word pairs and their associated vector representations \(\{x_i, z_i\}^n_{i=1}\), where \(x_i \in R^d_{1}\) is the distributional representation of word \textit{i} in the source language and \(z_i \in R^d_{2}\) is the vector representation of its translation.

The goal is to find a translation matrix \textit{W} such that \(Wx_i\) approximates \(z_i\). \textit{W} can be learned through several optimization problems. The one proposed by \cite{Mikolov2013translation} is the following:

\[\min_{W} \sum^{n}_{i=1} \parallel x_i - z_i \parallel^2 \]

Other optimization objectives will also be studied.

I will compare this approach to comparable monolingual SA systems and SMT-transfer methods. If the performance of my approach is close to that of the other two systems, it will show that this is a valid method to perform CLSC. If this method does not work, I will have to devise a new mapping strategy. Several possibilities include using a small parallel corpus or using different learning algorithms to learn the weight matrices.
	
\section{Work Plan}

$\begin{array}{cccccccccccccc}
 &  Jan  Feb  Mar  Apr  May  June  July  Aug  Sep  Oct  Nov Dec  \\ 
 2016  &  &  &  &  &  &  &  &  &  &  &  &\\ 
 2017  &  &  &  &  &  &  &  &  &  &  &  &\\ 
 2018  &  &  &  &  &  &  &  &  &  &  &  & 
\end{array} $


\printbibliography

\end{document}