\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Introduction}{3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Review of the Literature}{5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Sentiment Analysis}{5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Cross-lingual Sentiment Analysis}{6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}Neural Distributional Representation of Words}{10}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4}Deep Learning}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.5}Aspect-level Sentiment Analysis}{16}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Objectives}{17}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}General Objectives}{18}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Specific Objectives}{18}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Theoretical Framework}{19}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Empirical NLP}{19}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}Machine Learning}{19}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Hypotheses}{19}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Research Methodology}{20}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Adopted Methodology}{20}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Tools}{20}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Work Plan}{24}
