\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Introduction}{8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Literature Survey}{9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Sentiment Analysis}{9}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.1}Unsupervised Sentiment Analysis}{10}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.2}Supervised Machine Learning with Feature Engineering}{10}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.1.3}Deep Learning}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Cross-lingual Sentiment Analysis}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.1}New Resources via Machine Translation}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.2}Machine Translation for a Bilingual View}{14}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.3}Latent Representation}{14}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}Aspect-level Sentiment Analysis}{15}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Objectives}{16}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Theoretical Framework}{17}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Distributional Semantics}{17}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.1}Background}{17}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.2}Count Models versus Predict Models}{17}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.3}Advantages}{18}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.4}Limitations}{19}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.5}Alternatives}{20}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}Machine Learning}{21}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.1}Background}{21}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.2}Advantages}{21}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.3}Limitations}{21}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Hypotheses}{22}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Research Methodology}{22}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Adopted Methodology}{22}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.1.1}Datasets}{22}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.1.2}Tools}{24}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Preliminary Experiments}{25}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1}Zero-shot Learning for CLSA}{25}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.1.1}Data Sets}{26}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.1.2}Experiment 1}{27}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Layout}{27}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.1.3}Experiment 2}{30}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.1.4}Experiment 3}{31}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.1.5}Experiment 4}{32}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.1.6}Discussion and Further Research}{33}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2}Bilingual Word-embeddings for CLSA}{34}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.2.1}Experiment}{34}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.2.2}Discussion and Further Research}{36}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.3}Stacked Denoising Autoencoders for CLSA}{36}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.3.1}Experiment}{36}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {7.3.2}Discussion and Further Research}{37}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8}Work Plan}{38}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {9}Explicit References}{39}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.1}Zhou et al. (2016)}{39}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.2}Liu (2012)}{40}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.3}Gouws et al. (2015)}{40}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.4}Klementiev et al. (2012)}{41}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.5}Chandar et al. (2014)}{41}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {10}References}{42}
