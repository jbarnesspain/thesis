\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Introduction}{8}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Literature Survey}{9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Aspect-level Sentiment Analysis}{9}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Sentiment Analysis}{10}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.1}Unsupervised Sentiment Analysis}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.2}Supervised Machine Learning with Feature Engineering}{11}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.3}Deep Learning}{12}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}Cross-lingual Sentiment Analysis}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.1}Aspect-based CLSA}{13}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.2}New Resources via Machine Translation}{15}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.3}Machine Translation for a Bilingual View}{15}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.4}Latent Distributed Representations}{16}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Objectives}{17}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Main Goals}{17}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Specific Objectives}{17}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Theoretical Framework}{18}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Distributional Semantics}{18}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.1}Background}{18}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.2}Count Models versus Predict Models}{18}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.3}Advantages}{19}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.4}Limitations}{19}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.1.5}Alternatives}{21}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}Machine Learning}{21}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.1}Background}{21}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.2}Advantages}{22}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.2.3}Limitations}{22}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Hypotheses}{22}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Research Methodology}{23}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Adopted Methodology}{23}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2}Work Packages}{24}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.2.1}Datasets}{26}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.2.2}Tools}{27}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Work Plan}{29}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8}Completed Experiments}{30}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.1}Zero-shot Learning for CLSA}{30}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {8.1.1}Data Sets}{30}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {8.1.2}Experiment 1: English-Catalan zero-shot mapping with linear matrix and feedforward network}{31}
\defcounter {refsection}{0}\relax 
\contentsline {subparagraph}{Layout}{31}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {8.1.3}Experiment 2: English-Spanish zero-shot mapping with linear matrix and feedforward network}{34}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {8.1.4}Experiment 3: English-Spanish zero-shot mapping with linear matrix and LSTM}{35}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {8.1.5}Experiment 4: English-Spanish zero-shot mapping with neural matrix and feedforward network}{37}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {8.1.6}Discussion and Further Research}{38}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.2}Bilingual Word-embeddings for CLSA}{39}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {8.2.1}Experiment}{39}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {8.2.2}Discussion and Further Research}{40}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.3}Stacked Denoising Autoencoders for CLSA}{41}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {8.3.1}Experiment}{41}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {8.3.2}Discussion and Further Research}{41}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {8.4}Final Comparison}{42}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {9}Explicit References}{43}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.1}Zhou et al. (2016)}{43}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.2}Liu (2012)}{43}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.3}Gouws et al. (2015)}{44}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.4}Klementiev et al. (2012)}{44}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {9.5}Chandar et al. (2014)}{45}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {10}References}{45}
