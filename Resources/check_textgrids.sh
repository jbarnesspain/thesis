#!/bin/bash

#This script checks to see if all the textgrids in two folders are different

$1=='folder1'
$2=='folder2'

echo "First folder: $1"
echo "Second folder: $2"

echo diff <(ls -1a $1) <(ls -1a $2)

