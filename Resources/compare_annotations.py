import numpy as np
import copy, re, os

def get_between(left, right, text):
    no_left = re.split(left, text)[1]
    return re.split(right, no_left)[0].strip()

def get_token_section(KAF):
    return get_between('<text>', '</text>', KAF)

def get_tokens(token_section):
    tokens = {}
    for tok in re.split('<wf', token_section):
        try:
            wid = re.findall('wid="w_([0-9]*)"', tok)[0]
            word = re.findall('sent="[0-9]*">(.*)</wf>', tok)[0]
            tokens[int(wid)] = word
        except IndexError:
            pass
    return tokens

def get_num_tokens(KAF):
    return max([int(w) for w in re.findall('wid="w_([0-9]*)"', KAF)])

def get_opinion_section(annotation):
    """Gets opinion annotations from KAF file"""
    if len(re.findall('<opinions>', annotation)) > 0:    
        return get_between('<opinions>', '</opinions>', annotation)

def get_opinions(opinion_section):
    """Splits opinion section into seperate opinions"""
    if opinion_section != None:
        opinions = []
        for i in re.split('</opinion>', opinion_section):
            opinions.append(i)
        return opinions[:-1]
    else:
        return None

def get_opinion_id(opinion):
    """Gets tag id of opinion"""
    if opinion != None:
        tag_id = re.findall('<opinion oid="o([0-9]*)">', opinion)[0]
        return int(tag_id)
    else:
        return None

def get_opinion_holder(opinion):
    """Gets word ids of opinion holder"""
    
    if len(re.findall('<opinion_holder>', opinion)) > 0:
        oh = get_between('<opinion_holder>', '</opinion_holder>', opinion)
        oh_ids = re.findall('<target id="t_([0-9]*)"', oh)
        return [int(oh_id) for oh_id in oh_ids]
    else:
        return None

def get_opinion_target(opinion):
    """Gets word ids of opinion target"""
    
    if len(re.findall('<opinion_target>', opinion)) > 0:
        ot = get_between('<opinion_target>', '</opinion_target>', opinion)
        ot_ids = re.findall('<target id="t_([0-9]*)"', ot)
        return [int(ot_id) for ot_id in ot_ids]
    else:
        return None

def get_opinion_expression(opinion):
    """Gets word ids and polarity of opinion (strong positive,
       positive, negative, or strong negative)."""
    
    if len(re.findall('<opinion_expression', opinion)) > 0:
        polarity = re.findall('<opinion_expression polarity="(.*)">', opinion)[0]
        oe = get_between('<opinion_expression', '</opinion_expression>', opinion)
        oe_ids = [int(oe_id) for oe_id in re.findall('<target id="t_([0-9]*)"', oe)]
        return [(oe_id, polarity) for oe_id in oe_ids]
    else:
        return None



class KafOpinion(object):

    def __init__(self, KAF):
        self.KAF = KAF
        self.tokens_section = get_token_section(self.KAF)
        self.tokens = get_tokens(self.tokens_section)
        self.num_tokens = get_num_tokens(self.KAF)
        self.opinion_section = get_opinion_section(self.KAF)
        self.raw_opinions = get_opinions(self.opinion_section)
        self.opinions = self.parse_opinions(self.raw_opinions, self.num_tokens)


    def parse_opinions(self, raw_opinions, num_toks_in_doc):
        opinions = []
        if raw_opinions != None:
            for opinion in raw_opinions:
                opinion_id = get_opinion_id(opinion)
                opinion_holder = get_opinion_holder(opinion)
                opinion_target = get_opinion_target(opinion)
                opinion_expression = get_opinion_expression(opinion)
                this_op = Opinion(num_toks_in_doc, opinion_id, opinion_holder,
                                  opinion_target, opinion_expression)
                opinions.append(this_op)
        else:
            opinion_id = None
            opinion_holder = None
            opinion_target = None
            opinion_expression = None
            this_op = Opinion(num_toks_in_doc, opinion_id, opinion_holder,
                              opinion_target, opinion_expression)
            opinions.append(this_op)
        return opinions
        

class Opinion(object):

    def __init__(self, num_tokens_in_doc, opinion_id, opinion_holder,
                 opinion_target, opinion_expression):

        self.num_toks = num_tokens_in_doc
        self.opinion_id = opinion_id
        self.opinion_holder = opinion_holder
        self.opinion_target = opinion_target
        self.opinion_expression = opinion_expression
        
###########################################################################
###########################################################################
        #Create sentence vectors

opinion_dict = {'StrongNegative': 1,
	  'Negative': 2,
	  'Positive': 4,
	  'StrongPositive': 5}

def create_sent_array(num_tokens):
    """Creates an array of zeros of the length of the tokens
       in the sentence."""
    
    return np.array(np.zeros((num_tokens,1)))

def create_opinion_holder_array(sent_array, word_ids):
    """We are using the word 'holder' identify the span of
       the opinion holders in the sentence array"""
    new = copy.deepcopy(sent_array)
    for i in word_ids:
       new[i-1] = 1
    return new

def create_opinion_target_array(sent_array, word_ids):
    """We are using the word 'target' to identify the span of
       the opinion targets in the sentence array"""
    new = copy.deepcopy(sent_array)
    try:
        for i in word_ids:
            new[i-1] = 1
        return new
    except TypeError:
        return sent_array
    

def create_opinion_array(sent_array, word_ids_and_opinions):
    """We are using the to identify the span
       the opinions and their opinion in the sentence array"""
    new = copy.deepcopy(sent_array)
    for w_ids, op in word_ids_and_opinions:
        new[w_ids-1] = opinion_dict[op]
        
    return new
#############################################################################
#############################################################################

def mean_sqr_error(a, b):
	assert len(a) == len(b)
	n = len(a)
	return float(sum((a-b)**2))/n

def f1_score(a, b):
	assert len(a) == len(b)
	prec = len([n for i,n in enumerate(a) if n == b[i]])/len(b)
	recall = len([n for i,n in enumerate(a) if n == b[i]])/len(a)
	return (prec+recall)/2
    
def compare_opinion_expressions(KAF1, KAF2, error_function):
    ko1 = KafOpinion(KAF1)
    ko2 = KafOpinion(KAF2)
    
    opinion_expressions1 = []
    opinion_expressions2 = []

    for opinion in ko1.opinions:
        try:
            opinion_expressions1.extend(opinion.opinion_expression)
        except TypeError:
            pass
    for opinion in ko2.opinions:
        try:
            opinion_expressions2.extend(opinion.opinion_expression)
        except TypeError:
            pass

    sent_array = create_sent_array(ko1.num_tokens)
    array1 = create_opinion_array(sent_array, opinion_expressions1)
    array2 = create_opinion_array(sent_array, opinion_expressions2)

    return error_function(array1, array2)

def open_kafs(DIR):
    file_names = os.listdir(DIR)
    kafs = [open(DIR+fn).read() for fn in sorted(file_names) if fn.endswith('.kaf')]
    return kafs

def compare_opinion_lists(List1, List2, error_function):

    errors = []
    for kaf1,kaf2 in list(zip(List1, List2)):
        try:
            err = compare_opinion_expressions(kaf1, kaf2, error_function)
            errors.append(err)
        except IndexError:
            pass

    return sum(errors)/float(len(errors))

#####################################################################################    

def compare_opinion_targets(KAF1, KAF2, error_function):
    ko1 = KafOpinion(KAF1)
    ko2 = KafOpinion(KAF2)
    
    opinion_targets1 = []
    opinion_targets2 = []

    for opinion in ko1.opinions:
        try:
            opinion_targets1.extend(opinion.opinion_target)
        except TypeError:
            pass
        
    for opinion in ko2.opinions:
        try:
            opinion_targets2.extend(opinion.opinion_target)
        except TypeError:
            pass

    sent_array = create_sent_array(ko1.num_tokens)
    array1 = create_opinion_target_array(sent_array, opinion_targets1)
    array2 = create_opinion_target_array(sent_array, opinion_targets2)

    return error_function(array1, array2)


def compare_targets_lists(List1, List2, error_function):

    errors = []
    for kaf1,kaf2 in list(zip(List1, List2)):
        try:
            err = compare_opinion_targets(kaf1, kaf2, error_function)
            errors.append(err)
        except IndexError:
            pass

    return sum(errors)/float(len(errors))

#######################################################################################

def compare_opinion_holders(KAF1, KAF2, error_function):
    ko1 = KafOpinion(KAF1)
    ko2 = KafOpinion(KAF2)
    
    opinion_holders1 = []
    opinion_holders2 = []

    for opinion in ko1.opinions:
        try:
            opinion_holders1.extend(opinion.opinion_holder)
        except TypeError:
            pass
        
    for opinion in ko2.opinions:
        try:
            opinion_holders2.extend(opinion.opinion_holder)
        except TypeError:
            pass

    sent_array = create_sent_array(ko1.num_tokens)
    array1 = create_opinion_target_array(sent_array, opinion_holders1)
    array2 = create_opinion_target_array(sent_array, opinion_holders2)

    return error_function(array1, array2)

def compare_holder_lists(List1, List2, error_function):

    errors = []
    for kaf1,kaf2 in list(zip(List1, List2)):
        try:
            err = compare_opinion_holders(kaf1, kaf2, error_function)
            errors.append(err)
        except IndexError:
            pass

    return sum(errors)/float(len(errors))

############################################################################



def get_number_of_instances(KAFS):

    opinion_count = 0
    opinion_target = 0
    opinion_holder = 0

    
    for kaf in KAFS:
        ko = KafOpinion(kaf)
        for opinion in ko.opinions:
            if opinion.opinion_expression != None:
                opinion_count += 1
            if opinion.opinion_target != None:
                opinion_target += 1
            if opinion.opinion_holder != None:
                opinion_holder += 1

    return opinion_count, opinion_target, opinion_holder

def get_number_tokens(KAFS):
    num_tokens = 0
    for kaf in KAFS:
        ko = KafOpinion(kaf)
        tokens = ko.num_tokens
        num_tokens += tokens
    return num_tokens

def print_iaa_stats(List1, List2):
    num_tokens = get_number_tokens(List1)
    opinion_count1, opinion_targets1, opinion_holders1 = get_number_of_instances(List1)
    opinion_count2, opinion_targets2, opinion_holders2 = get_number_of_instances(List2)
    
    opinion_sqr_error = compare_opinion_lists(List1, List2, mean_sqr_error)
    opinion_f1 = compare_opinion_lists(List1, List2, f1_score)
    target_error = compare_targets_lists(List1, List2, f1_score)
    holder_error = compare_holder_lists(List1, List2, f1_score)

    print('+'*60)
    print('CORPUS STATISTICS')
    print('+'*60)
    print()
    print('Number of KAFs analyzed: {0}'.format(len(List1)))
    print('Number of tokens: {0}'.format(num_tokens))
    print()
    print('+'*60)
    print()
    print('ANNOTATOR ONE:')
    print('-'*60)
    print('Opinion Count: {0}'.format(opinion_count1))
    print('Targets:       {0}'.format(opinion_targets1))
    print('Holders:       {0}'.format(opinion_holders1))
    print()
    print('+'*60)
    print()
    print('ANNOTATOR TWO:')
    print('-'*60)
    print('Opinion Count: {0}'.format(opinion_count2))
    print('Targets:       {0}'.format(opinion_targets2))
    print('Holders:       {0}'.format(opinion_holders2))
    print()
    print('+'*60)
    print()
    print('Mean Squared Error for Opinions:     {:03.3f}'.format(opinion_sqr_error))
    print('F1-Score for Opinions:               {:03.3f}'.format(opinion_f1))
    print('F1 score for Opinion Targets:        {:03.3f}'.format(target_error))
    print('F1 score for Opinion Holder:         {:03.3f}'.format(holder_error))
    print()
    print('+'*60)
    print('+'*60)

def remove_problems(kafs):
    kafs.remove(kafs[166])
    kafs.remove(kafs[177])
    kafs.remove(kafs[182])
    kafs.remove(kafs[199])
    kafs.remove(kafs[246])
    return kafs

if __name__ == "__main__":

    #The use of f1 and mean squared error follow
    #Schouten and Fransincar (2015)
    #Precision and Recall are not given as the precision
    #of one annotator is the recall of the other. 
    
    jerdir = "/home/jeremy/NS/Keep/Temp/Thesis/Annotate_Cat_Corpus/Compare_Annotations/Jeremy-compare-Patrik/"
    patdir = "/home/jeremy/NS/Keep/Temp/Thesis/Annotate_Cat_Corpus/Compare_Annotations/Patrik-tag-to-kaf/"

    jerkafs = open_kafs(jerdir)
    patkafs = open_kafs(patdir)

    jerkafs = remove_problems(jerkafs)
    patkafs = remove_problems(patkafs)

    print_iaa_stats(jerkafs, patkafs)

    tonidir = "/home/jeremy/NS/Keep/Temp/Thesis/Annotate_Cat_Corpus/Compare_Annotations/Toni-tag-to-kaf/"
    jer2dir = "/home/jeremy/NS/Keep/Temp/Thesis/Annotate_Cat_Corpus/Compare_Annotations/Jeremy-Toni-tag-to-kaf/"

    tonikafs = open_kafs(tonidir)
    jer2kafs = open_kafs(jer2dir)

    print_iaa_stats(jer2kafs, tonikafs)

    
