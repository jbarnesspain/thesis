import os, nltk, re, sys
sys.path.append('/home/jeremy/Documentos/Corpora/Resources/')
from FreelingParser import *
import gensim, logging

class MySentences(object):
    """For a corpus that has a number of subfolders, each
    containing a set of text files. Supposes that in each text file,
    there is one sentence per line. Yields one tokenized sentence
    at a time."""
    
    def __init__(self, dirname, encoding='utf8'):
        self.dirname = dirname
        self.encoding = encoding #added encoding parameter for non utf8 texts
        
    def __iter__(self):
        for dname in os.listdir(self.dirname):
            sub_dir = os.path.join(self.dirname, dname)
            for fname in os.listdir(sub_dir):
                text_file = os.path.join(sub_dir, fname)
                for line in open(text_file, encoding=self.encoding):
                    line = re.sub('<.*?>', '', line)
                    if dname.isupper():
                        yield fe.tokenize_text(line) #you can change tokenizer
                    else:
                        yield fp.tokenize_text(line)
    

            
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
fp = FreelingParser('ca')
fe = FreelingParser('en')
sents = MySentences("/home/jeremy/Escritorio/Dual_wiki/")
