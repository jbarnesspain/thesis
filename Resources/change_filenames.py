import sys
import os



change_dic = {'à':'a',
              'á':'a',
              'è':'e',
              'é':'e',
              'í':'i',
              'ì':'i',
              'ó':'o',
              'ò':'o',
              'ú':'u',
              'ù':'u',
              'ü':'u',
              'À':'A',
              'Á':'A',
              'È':'E',
              'É':'E',
              'Ì':'I',
              'Í':'I',
              'Ò':'O',
              'Ó':'O',
              'Ù':'U',
              'Ú':'U',
              'Ü':'U',
              'ª':'a',
              'ç':'c',
              'l·l':'ll',
              'ñ':'n'
              }

def replace_sym(filename):
    new = ''
    for letter in filename:
        if letter in change_dic.keys():
            new += change_dic[letter]
        else:
            new += letter
    return new

def get_files_names(DIR):
    filenames = os.listdir(DIR)
    full_filenames = [DIR+filename for filename in filenames]
    files = [open(full_filename).read() for full_filename in full_filenames]
    return list(zip(filenames, files))

def change_filenames(DIR, OUT):

    fnames_files = get_files_names(DIR)
    new = []
    for fn, file in fnames_files:
        new_name = replace_sym(fn)
        new.append((new_name, file))

    for fn, file in new:
        with open(OUT+fn, 'w') as handle:
            handle.write(file)
            
