import os, nltk, re, sys, nltk
sys.path.append('/home/jeremy/Documentos/Corpora/Resources/')
from FreelingParser import *
import gensim, logging

class MySentences(object):
    """For a corpus that has a number of subfolders, each
    containing a set of text files. Supposes that in each text file,
    there is one sentence per line. Yields one tokenized sentence
    at a time."""
    
    def __init__(self, dirname, encoding='utf8'):
        self.dirname = dirname
        self.encoding = encoding #added encoding parameter for non utf8 texts
        
    def __iter__(self):
        for fname in os.listdir(self.dirname):
            sub_dir = os.path.join(self.dirname, fname)
            for fname in os.listdir(sub_dir):
                text_file = os.path.join(sub_dir, fname)
                for line in open(text_file, encoding=self.encoding):
                    line = re.sub('<.*?>', '', line)
                    #yield nltk.word_tokenize(line, language='english') #you can change tokenizer
                    yield fp.tokenize_text(line)
    


class Corpus_Stats(object):
    """For a corpus that has a number of subfolders, each
    containing a set of text files. Supposes that in each text file,
    there is one sentence per line. Yields one tokenized sentence
    at a time."""
    
    def __init__(self, dirname, lang='ca', encoding='utf8'):
        self.dirname = dirname
        self.encoding = encoding #added encoding parameter for non utf8 texts
        self.parser = FreelingParser(lang)
        
    def print_stats(self):
        
        num_files = 0
        num_sents = 0
        num_tokens = 0
        fd = nltk.FreqDist()
        
        for fname in os.listdir(self.dirname):
            sub_dir = os.path.join(self.dirname, fname)
            for fname in os.listdir(sub_dir):
                text_file = os.path.join(sub_dir, fname)
                num_files += 1
                if num_files % 50 == 0:
                    print('Number of files analyzed: {0}'.format(num_files))
                for line in open(text_file, encoding=self.encoding):
                    line = re.sub('<.*?>', '', line)
                    num_sents +=1
                    """if num_sents % 10000 == 0:
                       print('Sents: {0}'.format(num_sents))"""
                    #yield nltk.word_tokenize(line, language='english') #you can change tokenizer
                    tokens = self.parser.tokenize_text(line)
                    num_tokens += len(tokens)
                    """if num_tokens % 100000 == 0:
                        print('Tokens: {0}'.format(num_tokens))"""
                    fd.update(tokens)
        print('-'*80)
        print('Number of files in corpus:      {0}'.format(num_files))
        print('Number of sentences in corpus:  {0}'.format(num_sents))
        print('Number of tokens in corpus:     {0}'.format(num_tokens))
        print('Most Common Words: {0}'.format(fd.most_common(10)))

    def get_most_common_words(self, number):
        
        num_files = 0
        num_sents = 0
        num_tokens = 0
        fd = nltk.FreqDist()
        
        for fname in os.listdir(self.dirname):
            sub_dir = os.path.join(self.dirname, fname)
            for fname in os.listdir(sub_dir):
                text_file = os.path.join(sub_dir, fname)
                num_files += 1
                if num_files % 50 == 0:
                    print('Number of files analyzed: {0}'.format(num_files))
                for line in open(text_file, encoding=self.encoding):
                    line = re.sub('<.*?>', '', line)
                    num_sents +=1
                    """if num_sents % 10000 == 0:
                       print('Sents: {0}'.format(num_sents))"""
                    #yield nltk.word_tokenize(line, language='english') #you can change tokenizer
                    tokens = self.parser.tokenize_text(line)
                    num_tokens += len(tokens)
                    """if num_tokens % 100000 == 0:
                        print('Tokens: {0}'.format(num_tokens))"""
                    fd.update(tokens)
        return fd.most_common(number)

def sentencePerLine(dirname, lang='english', encoding='utf8'):

    for fname in os.listdir(dirname):
        sub_dir = os.path.join(dirname, fname)
        for fname in os.listdir(sub_dir):
            text_file = open(os.path.join(sub_dir, fname),
                             encoding=encoding).read()
            outfile = open(os.path.join(sub_dir, fname), 'w')
            for sentence in nltk.sent_tokenize(text_file, language=lang):
                    outfile.write(sentence+'\n')
            outfile.close()
            
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
fp = FreelingParser('ca')
sents = MySentences("/home/jeremy/Escritorio/cat_wiki/CatWiki/")
