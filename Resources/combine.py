from FreelingScript import *

def combine_tuples_chunks(tuples, chunks):

    #This will contain the final set of [number, label, xmin, xmax]
    final = []

    #set current search space
    current_search = tuples[:8]

    for i,label in enumerate(chunks):

        #set current chunk label and words associated with that chunk
        current_label, current_words = label[0], label[1:]

        #create new list to save results of search
        new_label = [i+1, current_label]


        #for loop that looks through current search space to see if there
        #is a match for words
        for num, xmin, xmax, w in current_search:
            if w == current_words[0]:
                new_label.append(xmin)
                ind = tuples.index((num,xmin,xmax,w))
                current_search = tuples[ind+1:ind+9]
                if len(new_label) == 4:
                    break
            if w == current_words[-1]:
                new_label.append(xmax)
                ind = tuples.index((num,xmin,xmax,w))
                current_search = tuples[ind+1:ind+9]
                if len(new_label) == 4:
                    break

        #Saves current label information in a final list of labels
        if len(new_label) == 4:
            final.append(new_label)
    return [(num, xmin, xmax, label) for (num, label, xmin, xmax) in final ]

def expand_word(word):
    l = tk.tokenize(word)
    return [w.get_form() for w in l]

def expand_tuples(tuples):

    final = []
        
    for (num, xmin, xmax, word) in tuples:
        ws = expand_word(word)
        if len(ws) > 1:
            for w in ws:
                final.append((num, xmin, xmax, w))
        else:
            final.append((num, xmin, xmax, word))
    return final
