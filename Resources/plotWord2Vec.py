import numpy as np
import matplotlib.pyplot as plt
from gensim.models import Word2Vec
from sklearn.manifold import TSNE

def getWordVecs(words, model, d=300):
    vecs = []
    words_in_model = []
    for word in words:
        word = word.replace('\n', '')
        try:
            vecs.append(model[word].reshape((1,d)))
            words_in_model.append(word)
        except KeyError:
            continue
    vecs = np.concatenate(vecs)
    return words_in_model, np.array(vecs, dtype='float')


def plot_word2vec(words, model, d=300, metric='euclidean', perplexity=30.0):
    """Plot 2-dim vectors of the words"""
    model_words, word_vecs = getWordVecs(words, model, d)
    ts = TSNE(2, perplexity, metric=metric)
    reduced_vecs = ts.fit_transform(word_vecs)
    print(len(reduced_vecs))

    fig = plt.figure()
    ax = fig.add_subplot(111)
    for i in range(len(reduced_vecs)):
        name = model_words[i]
        ax.text(reduced_vecs[i,0], reduced_vecs[i,1], name)

    plt.show()

if __name__ == '__main__':
    model = Word2Vec.load(
        "/home/jeremy/Documentos/LanguageModels/Catalan/300CatWindow7Negative10Model")
