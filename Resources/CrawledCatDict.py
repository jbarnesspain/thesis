import re, nltk, logging, pickle
import urllib.request
from sklearn.linear_model import SGDClassifier
from sklearn.cross_validation import train_test_split
from gensim.models import Word2Vec
import numpy as np

def get(url):
    """Acts like Mozilla Firefox asks for a page"""
    
    r = urllib.request.Request(url,headers={'User-agent':'Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11'})
    return str(urllib.request.urlopen(r).read().decode('utf8'))

def get_text_between(text,border_left,border_right):
    return text.split(border_left)[1].split(border_right)[0]

def create_dictionary(word_area):
    wordlist = re.findall('<b>(.*)</b>(.*)<br/>', word_area)
    wordlist = [(w, nltk.word_tokenize(tran)) for w, tran in wordlist]
    wordlist = [(w, tran[0]) for w, tran in wordlist]
    Dict = dict()
    for word, translation in wordlist:
            Dict[word] = translation
    return Dict

def crawlDictionary(urls):

    FinalDict = {}

    for url in urls:
        web = get(url)
        word_area = get_text_between(web, "<br/><br/><center><table ><tr><td ", "</td></tr></table>")
        FinalDict.update(create_dictionary(word_area))

    return FinalDict

def get_training_batch(fdict, EngModel, CatModel):
	X = []
	y = []
	for word, trans in fdict.items():
		try:
			xword = EngModel[word]
			yword = CatModel[trans]
			X.append(xword)
			y.append(yword)
		except KeyError:
			pass
	return X,y

def gradient_descent(alpha, X, y, ep=0.0001, maxiter=10000):
	converged = False
	iteration = 0
	m = X.shape[0] #number of samples

	#Transformation Matrix
	W = np.random.random((X.shape[1], X.shape[1]))
	for it in range(iteration, maxiter):
		for i,x in enumerate(X):
			x_transpose = x.T
			hypothesis = np.dot(W, x)
			loss = hypothesis - y[i]
			J = np.sum(loss ** 2) / (2*m)
			if it % 200 == 0:
				print('iter %s | J: %.3f' % (it, J))
			gradient = np.dot(x_transpose, loss) / m
			W = W - alpha * gradient
	return W

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def sigmoid_gradient(output):
    return output*(1-output)

def NeuralNetwork(alpha, X, y, hidden_dim=1000, maxiter=10000):
    np.random.seed(1)
    
    synapse_0 = 2*np.random.random((300, hidden_dim)) -1
    synapse_1 = 2*np.random.random((hidden_dim, 300)) -1

    
    for j in range(maxiter):
        layer_0 = Xtrain
        layer_1 = sigmoid(np.dot(layer_0, synapse_0))
        layer_2 = sigmoid(np.dot(layer_1, synapse_1))
        layer_2_error = layer_2 - ytrain
        if j% 100 == 0:
            print('error at {0} iter is: {1}'.format(j, sum(sum(layer_2_error))))
        layer_2_delta = layer_2_error * sigmoid_gradient(layer_2)
        layer_1_error = layer_2_delta.dot(synapse_1.T)
        layer_1_delta = layer_1_error*sigmoid_gradient(layer_1)
        synapse_1 -= alpha * (layer_1.T.dot(layer_2_delta))
        synapse_0 -= alpha * (layer_0.T.dot(layer_1_delta))

    return synapse_0, synapse_1

def cosign_dis(v1, v2):
    top = v1.dot(v2.T)
    bottom = (np.sqrt(sum(v1**2)))*(np.sqrt(sum(v2**2)))
    return top / bottom

def get_n_closest(word, Engmodel, Catmodel, synapse_0, synapse_1, n=5, restrict_vocab=5000):
    x = Engmodel[word]
    l1 = sigmoid(np.dot(x, synapse_0))
    l2 = sigmoid(np.dot(l1, synapse_1))

    mylist = [(cosign_dis(l2, Catmodel[key]), key) for key in list(catalan.vocab.keys())[:restrict_vocab]]
    mylist.sort()
    return mylist[-n:]

if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s : %(message)s : %(levelname)s', level=logging.INFO)
    urls = ['http://www.dicts.info/vocabulary/?l1=catalan&group=color', 'http://www.dicts.info/vocabulary/?l1=catalan&group=number', 'http://www.dicts.info/vocabulary/?l1=catalan&group=anatomy', 'http://www.dicts.info/vocabulary/?l1=catalan&group=animal', 'http://www.dicts.info/vocabulary/?l1=catalan&group=time', 'http://www.dicts.info/vocabulary/?l1=catalan&group=food', 'http://www.dicts.info/vocabulary/?l1=catalan&group=drink', 'http://www.dicts.info/vocabulary/?l1=catalan&group=fruit', 'http://www.dicts.info/vocabulary/?l1=catalan&group=vegetable', 'http://www.dicts.info/vocabulary/?l1=catalan&group=family', 'http://www.dicts.info/vocabulary/?l1=catalan&group=house', 'http://www.dicts.info/vocabulary/?l1=catalan&group=furniture', 'http://www.dicts.info/vocabulary/?l1=catalan&group=clothes', 'http://www.dicts.info/vocabulary/?l1=catalan&group=nature', 'http://www.dicts.info/vocabulary/?l1=catalan&group=city', 'http://www.dicts.info/vocabulary/?l1=catalan&group=transport', 'http://www.dicts.info/vocabulary/?l1=catalan&group=profession', 'http://www.dicts.info/vocabulary/?l1=catalan&group=geography', 'http://www.dicts.info/vocabulary/?l1=catalan&group=weather', 'http://www.dicts.info/vocabulary/?l1=catalan&group=material', 'http://www.dicts.info/vocabulary/?l1=catalan&group=container', 'http://www.dicts.info/vocabulary/?l1=catalan&group=tool', 'http://www.dicts.info/vocabulary/?l1=catalan&group=mathematics', 'http://www.dicts.info/vocabulary/?l1=catalan&group=abstract', 'http://www.dicts.info/vocabulary/?l1=catalan&group=adjective-basic', 'http://www.dicts.info/vocabulary/?l1=catalan&group=verb-basic', 'http://www.dicts.info/vocabulary/?l1=catalan&group=conjunction', 'http://www.dicts.info/vocabulary/?l1=catalan&group=preposition', 'http://www.dicts.info/vocabulary/?l1=catalan&group=pronoun']
    english = Word2Vec.load_word2vec_format('/home/jeremy/Documentos/Corpora/wikipedia_dumps/GoogleNews-vectors-negative300.bin.gz', binary=True)
    catalan = Word2Vec.load('/home/jeremy/Documentos/Corpora/cat_wiki/model300')

    with open('/home/jeremy/Documentos/CatalanEnglishDictionary.pickle', 'rb') as handle:
        FinalDict = pickle.load(handle)
    X,y = get_training_batch(FinalDict, english, catalan)
    
    Xtrain, Xtest = train_test_split(X)
    ytrain, ytest = train_test_split(y)
    Xtrain = np.array(Xtrain)
    ytrain = np.array(ytrain)

    syn0, syn1 = NeuralNetwork(0.01, Xtrain, ytrain, hidden_dim = 500, maxiter=1500)
    
