#!/bin/bash


for f in /home/jeremy/Documentos/Corpora/Corpus_Catala/Tokenized/out/*.txt
do
	name=${f##*/}
	base=${name%.txt}
	echo "Processing $base file..."
	cat $f | java -jar /home/jeremy/ixa-pipe-tok/target/ixa-pipe-tok-1.8.4.jar tok  --l es --notok > /home/jeremy/Documentos/Corpora/Corpus_Catala/Tokenized/Naf/$base.naf
	
	
done
