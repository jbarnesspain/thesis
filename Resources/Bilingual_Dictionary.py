import operator



class Bilingual_Dictionary():
    """This class creates a one way bilingual dictionary
    based on the dictionary types from www.dicts.info.

    Required format:

    [original terms] \t [translation terms] \t [gloss]

    If there are several original or translation terms, they
    must be seperated by a ';'

    ex.

    almost ; nearly \t casi ; aproximadamente \tadverb \n
    """
    
    def __init__(self, fname):
        self.dict = self.process_dictionary(fname)
    def process_dictionary(self, fname):
        my_dictionary = {}
        lines = open(fname).readlines()[29:]
        for line in lines:
            original_term, trans_term, info = line.strip().split('\t')
            original_terms = [w.strip() for w in original_term.split(';')]
            trans_terms = [w.strip() for w in trans_term.split(';')]
            if len(original_terms) > 1:
                for term in original_terms:
                    my_dictionary[term] = (trans_terms, info)
            else:
                my_dictionary[original_term] = (trans_terms, info)
        return my_dictionary

def ambiguity(word, bi_dic):
    return len(bi_dic[word][0])

def ambiguity_scores(bi_dict, sort=False):
    """This is a simple method of determining the amount
    of lexical ambiguity. We consider a word more ambiguous
    if it has more translations"""

    scores = {}
    for w in bi_dict.keys():
        scores[w] = ambiguity(w, bi_dict)
    if sort == True:
        return sorted(scores.items(), key=operator.itemgetter(1), reverse=True)
    else:
        return scores
    
