import sys, os
sys.path.append('/home/jeremy/Documentos/Corpora/Resources/')
from FreelingParser import *
import re
import gensim, logging



class MySentences(object):
    def __init__(self, dirname, encoding='utf8'):
        self.dirname = dirname
        self.encoding = encoding

    def __iter__(self):
        for fname in os.listdir(self.dirname):
            for line in open(os.path.join(self.dirname, fname),
                             encoding=self.encoding):
                yield fp.tokenize_text(line)


logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
myDIR = "/home/jeremy/Escritorio/cat_wiki/raw.ca/"
fp = FreelingParser()
sents = MySentences(myDIR, encoding='latin1')
#model = gensim.models.Word2Vec(sents, min_count=2, workers=4)

    
