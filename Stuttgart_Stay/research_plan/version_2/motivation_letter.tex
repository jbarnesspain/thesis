\documentclass[11pt]{article}
\usepackage{times}
\usepackage{url}
\usepackage{inputenc}
\usepackage{latexsym}
\usepackage{xcolor}
\usepackage{amssymb}
\usepackage[natbib=true, backend=biber, style=authoryear-comp, isbn=false, url=false, doi=false, maxcitenames=2, maxbibnames=10, uniquelist=false]{biblatex}
\usepackage[]{csquotes}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{sectsty}
\linespread{1.5}
\usepackage[section]{placeins}
\addbibresource{research_plan.bib}
\bibliography{research_plan}



\title{Cross-lingual Distributional Representations for Sentiment, Emotion and Abstraction}

\author{Jeremy Barnes \\
Universitat Pompeu Fabra\\
Roc Boronat 138\\
Barcelona, Spain\\
{\tt jeremy.barnes@upf.edu}
}
\date{}

\begin{document}
\maketitle


\section{Motivation}
\label{motivation}



The data created on the Internet have long been of interest to researchers, business leaders, and 
politicians. These data often contain information about private states, such as emotions, sentiment, or polarity. Finding ways to collect and harness this information has become the central task of several fields of study (sentiment analysis, emotion detection, irony/sarcasm detection), as well as influencing others (digital humanities, tracking political debates). 

For many languages, however, there are not enough resources to extract this information reliably. In these cases, we can use cross-lingual methods by which we leverage information available in one language to perform a task in an under-resourced language. Machine translation is the most common way to bridge the gap between two languages for sentiment analysis (\cite{Banea2013a}; \cite{Wan2009c}; \cite{Duh2011a}),⁠ but for many language combinations, machine translation is of poor quality or lacking entirely. The quality of machine translation directly affects the quality of the sentiment classifier (\cite{Balahur2014d}; \cite{Mohammad2015b})⁠. It is also unclear how phrase-based machine translation could be optimized for this task. For these reasons, it is necessary to investigate methods which can overcome these limitations. 

An alternative is to represent words using a distributional semantic model (DSM). The most frequently used DSMs are neural word embeddings (\cite{Mikolov2013word2vec}; \cite{Pennington2014})⁠, which factorize either an explicit or implicit co-occurence matrix. In fact, most state of the art sentiment analysis techniques use pre-trained DSMs as input or learn them during training (\cite{Tai2015a}; \cite{Kim2014})⁠⁠. However, these techniques cannot be used directly in cross-linguistic contexts, as in this case the data used to train the model comes from a different language than the test data. 

Learning a function to project information from a source DSM to a target DSM has proven possible and useful for certain tasks, such as filling gaps in translation dictionaries (\cite{Mikolov2013translation})⁠, or projecting images into semantic word spaces (\cite{Socher2013zeroshot})⁠, as well as for modeling regular changes in semantics based on morphology (\cite{Koeper2016})⁠. This technique could provide a solution to the data bottleneck in cross-lingual sentiment or emotion classification. Another advantage is that we can optimize this process to maintain the affective interpretation of the words. This is an advantage over machine translation, where it is much less clear how to incorporate a sentiment or emotion preserving structure.

Bilingual DSMs have become popular recently for many tasks in NLP (\cite{Klementiev2012b}; \cite{Zou2013}; \cite{Gouws2015}; \cite{Gouws2015taskspecific})⁠. There have also been some applications of bilingual DSMs to sentiment analysis (\cite{Chandar2013}; \cite{Zhou2015}; \cite{Jain2015})⁠ but all previous work has concentrated on document-level classification, whereas my research is concerned primarily with aspect/target-level classification. This is a promising research area because some approaches to create these representations are able to take advantage of both monolingual as well as parallel data (\cite{Faruqui2014}; \cite{Gouws2015}; \cite{Gouws2015taskspecific})⁠. These methods are particularly appropriate for under-resourced languages, but can also provide a simple solution for any language that does not have sentiment or emotion resources. However, their current performance for cross-lingual sentiment analysis is not optimal (\cite{Barnes2016}). Therefore, it is necessary to improve the current models for these tasks.


\section{Goals}


\subsection*{Goal 1: Create sentiment/emotion models for under-resourced languages}
One of the limitations that we currently face is that there are a large number of languages that do not have a significant number of NLP resources. It is curious to note that machine translation can only alleviate this situation when there are enough parallel resources, thus excluding many under-resourced languages.  As mentioned in section \ref{motivation}, distributed representations may provide an alternative method which performs better for sentiment analysis. Therefore, I propose to use distributed representations to create sentiment/emotion models for under-resourced languages. This goal includes the following subgoals:

\subsubsection*{Subgoal 1: Investigate how sentiment and emotion are represented in vector space}
Some research has shown that DSMs contain sentiment information, and that this information can be concentrated into a small number of dimensions (\cite{Rothe2016}). What is not clear is how sentiment and emotion are captured through co-occurence. If we understand how these concepts are represented, we should be able to understand more fully how they differ cross-lingually.  Additionally, it would be useful to compare different techniques for creating these vectors, in order to determine if we can optimize 

\subsubsection*{Subgoal 2: Investigate methods of projecting sentiment from source to target languages}
Machine translation has been the most common tool for transferring resources to new languages. As mentioned in Section \ref{motivation} , this has several limitations. Representing words in a cross-lingual vector space has the advantage that it is possible to search for similar words across languages, as well as perform arithmetical operations to reorder the space for a specific task. This would enable a simple transfer for languages with few parallel resources.

One option is to learn a function to project target-language vectors into trained source-language vector space. This would be straight-forward, as the only resource needed would be a task specific bilingual lexicon. 

Another option is to learn a function to project source and target-language vectors to a neutral space. This may reduce the loss that occurs from a direct mapping. 

\subsubsection*{Subgoal 3: Investigate how to enrich bilingual word embeddings with sentiment information}
From preliminary results, bilingual word embeddings are a promising method to perform cross-lingual sentiment analysis. The disadvantages, however, are that these representations are not optimized for sentiment and cannot be fine-tuned while training a classifier. In order to alleviate these problems I propose two methods.

The preferable manner to achieve this would be to retrofit pre-trained DSMs with the sentiment task in mind. One could finetune the source vectors to the task and then learn a function to replicate this in the target-language vectors. This technique has proven effective for reducing out-of-vocabulary errors on parsing (\cite{Madhyastha2016}).

If this first method does not give good results, we can proceed to incorporate sentiment information during the training of the bilingual vectors. This would require more experimental work and is already an active field of study (\cite{Tang2014}; \cite{Faruqui2014})

\subsubsection*{Subgoal 4: Evaluate methods on emotion classification}

Given that sentiment classification is a simpler task than emotion classification (sentiment classification usually involves fewer classes which move along a polarity scale, whereas emotion classification can have a number of classes which overlap to a degree), it would be beneficial to do preliminary tests on sentiment datasets in order to validate the technique. Once we have found a method that works, we can apply this to an emotion dataset and determine whether the technique is applicable in its current state or needs some adjustments.

\subsubsection*{Article 1}
Publish article in EMNLP or IJCNLP (deadline in June).
\subsubsection*{Article 2}
Publish second article, possibly in Journal of Natural Engineering or Transactions of the ACL (TACL).


\subsection*{Goal 2: Investigate if and how abstractness in sentiment and emotion can be represented in vector format}
Recent studies suggest that abstract terms are grounded in emotion (\cite{Vigliocco2009}; \cite{Skipper2014}). This begs the question: Can the knowledge of abstractness help disambiguate sentiment and emotion in text? \textcite{Turney2011} use abstractness to identify literal and metaphorical contexts. While this is not emotion or sentiment, it does show that abstractness can be used in order to improve classification of related concepts. 

\subsubsection*{Subgoal 1: Preliminary experiments}
We will seek to answer whether lexical ambiguity and abstractness can allow sentiment or emotion and determine to what degree these phenomena co-occur in sentiment and emotional text. This stage will require the collection of resources and an analysis of corpora. This analysis will determine to what extent this approach is feasible. 


\subsubsection*{Subgoal 2: Represent abstraction and ambiguity in sentiment in vectors}
The next stage is to determine what is the best way to represent abstractness and ambiguity in vector space for the task of sentiment analysis. We will evaluate these representations empirically to establish the validity of this approach.

\subsubsection*{Subgoal 3: Transfer these methods to emotion}
Depending on the success of the methods in subgoal 2, we will apply these to emotion detection.

\subsubsection*{Article 3}
Publish article in ACL (deadline in September 2017).


\subsection*{Goal 3: Investigate if and how the relationship between abstractness, sentiment and emotion differ between languages}
The third goal is to investigate whether the relationship between abstractness found in Goal 2 holds across languages. If it does hold, it may improve cross-lingual sentiment and emotion classification. If not, this relationship may still vary regularly in such a way that allows us to model it and exploit it to improve classification.

\subsubsection*{Subgoal 1: Collection of data}
The first goal of this stage will be to find datasets which allow comparison across languages. The availability or creation of such datasets will determine the next stages.

\subsubsection*{Subgoal 2: Preliminary experiments}
At this stage we will test the methods developed in Goal 2 on different languages. Depending on the these methods, it is likely that we will need either to use a current technique for cross-lingual transfer or to develop a new one. We will need to do a detailed examination of the errors.

\subsubsection*{Subgoal 3: Modeling the change}
If the results from these preliminary experiments show that the relationship does not hold across languages but is regular to some degree, it may be possible to model this shift to improve classification of sentiment and emotion.

\subsubsection*{Article 4}
Publish article in NAACL or LREC (deadline in February 2018).

\section{Workplan}


\begin{figure}[h!t]
\centering
\makebox[\textwidth][c]{\includegraphics[scale=1.1]{work_plan.png}}
\caption{Current Work Plan}
\label{work_plan}
\end{figure}

\section{References}
\printbibliography[heading=none]

\end{document}
